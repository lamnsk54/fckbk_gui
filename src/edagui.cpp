#include "edagui.h"
#include "ui_edagui.h"

edagui::edagui(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::edagui)
{
    ui->setupUi(this);
    dir = new QFileSystemModel(this);
    edatool = new QProcess;
    dir->setReadOnly(false);
    ui->treeView->setModel(dir);

    //dat duong dan den fck-bk-dbg, mac dinh la chua dat
    edatoolpath = "";
    //dat duong dan muon hien thi o tree view
    QModelIndex index = dir->setRootPath("/home");
    rootproject = dir->rootPath();
    ui->treeView->setRootIndex(index);
    ui->treeView->resizeColumnToContents(0);
    ui->treeView->hideColumn(1);
    ui->treeView->hideColumn(2);
    ui->treeView->hideColumn(3);
    ui->treeView->setHeaderHidden(true);
    //Cap nhat ten Treeview la ten project bang cach
    ui->nameproject->setTitle(rootproject.rightRef(rootproject.length()-rootproject.lastIndexOf("/")-1).toString());
    //loc nhung file muon hien thi o treeview
    QStringList filter;
    filter <<"*.v" <<"*.sv" <<"*.vcd" <<"*.txt" <<"*.dot";
    dir->setNameFilters(filter);
    dir->setNameFilterDisables(false);
    createStatusBar();
    //cai dat su kien an nut enter cho textedit_command
    key = new keyEnterReceiver(ui->textEdit_command,edatool);
    ui->textEdit_command->installEventFilter(key);
    //Tat cac thao tac khi chua co tab nao duoc mo
    ui->actionSave->setDisabled(true);
    ui->actionCut->setDisabled(true);
    ui->actionCopy->setDisabled(true);
    ui->actionPaste->setDisabled(true);
    ui->actionSave_As->setDisabled(true);
    //Tat cac thao tac khi chua chay FCKBK
    ui->actionCommandBoolector->setDisabled(true);
    ui->actionCommandMiniSat->setDisabled(true);
    ui->actionCommandRead_RTL->setDisabled(true);
    ui->actionCommandVerify->setDisabled(true);
    ui->actionCommandStop->setDisabled(true);
    ui->actionRead_Property->setDisabled(true);
    ui->actionPrint_Circuit->setDisabled(true);
    ui->actionPrint_Property->setDisabled(true);
    ui->actionPrint_IPC->setDisabled(true);

    QList<int> heights,heights_2;
    heights.push_back(150);
    heights.push_back(450);
    ui->splitter->setSizes(heights);
    heights_2.push_back(800);
    heights_2.push_back(200);
    ui->splitter_2->setSizes(heights_2);
}

edagui::~edagui()
{
    delete ui;
    delete dir;
    delete edatool;
    delete key;
}
//Cac phuong thuc xu ly van ban o control myqtextedit
//{ Kiem tra va dua ra lua chon co muon save cua so soan thao hien tai khong
bool edagui::maybeSave(int index)
{
    if (text[index]->document()->isModified())
    {
        QMessageBox::StandardButton ret;
        ret = QMessageBox::warning(this, "Application",
                                   "The document: " + ui->tabWidget->tabText(index) + " has been modified.\n"
                                      "Do you want to save your changes?",
                                   QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
        if (ret == QMessageBox::Save)
        {
            return on_actionSave_triggered();
        }
        else if (ret == QMessageBox::Cancel)
            return false;
    }
    return true;
}//} Kiem tra va dua ra lua chon co muon save cua so soan thao hien tai khong

//{ Mo mot file text
void edagui::loadFile(const QString &fileName)
{
    bool flagexist = false; //Kiem tra cac tab xem da ton tai file da mo chua
    QFile file(fileName);
    if (!file.open(QFile::ReadOnly | QFile::Text)) {
        QMessageBox::warning(this, tr("Application"),
                             tr("Cannot read file %1:\n%2.")
                             .arg(fileName)
                             .arg(file.errorString()));
        return;
    }
    QTextStream in(&file);

    for (int i = 0; i < text.size(); ++i)
    {
        //Kiem tra cac tab xem da ton tai file da mo chua
        if (fileName == text[i]->Getfilepath())
        {
            //focus vao tab
            ui->tabWidget->setCurrentWidget(ui->tabWidget->widget(i));
            flagexist = true;
        }
    }
    if (!flagexist)
    {
        int sizeoftab = ui->tabWidget->count();
        QWidget *windows = new QWidget;
        QHBoxLayout *layout = new QHBoxLayout;
        CodeEditor *tabtext = new CodeEditor;
        tabtext->setupEditor();
        layout->addWidget(tabtext);
        windows->setLayout(layout);
        QString textlabel = fileName;
        textlabel = textlabel.rightRef(textlabel.length()-textlabel.lastIndexOf("/")-1).toString();
        ui->tabWidget->insertTab(sizeoftab,windows,textlabel);
        ui->tabWidget->setCurrentWidget(windows);
        text.insert(sizeoftab,tabtext);
        tabtext->setPlainText(in.readAll());
        setCurrentFile(fileName,sizeoftab);

        ui->actionSave->setEnabled(true);
        ui->actionCut->setEnabled(true);
        ui->actionCopy->setEnabled(true);
        ui->actionPaste->setEnabled(true);
        ui->actionSave_As->setEnabled(true);
    }
    statusBar()->showMessage(tr("File loaded"), 2000);
}//} Mo mot file text

//{ Luu file text
bool edagui::saveFile(const QString &fileName,int index)
{
    QFile file(fileName);
    if (!file.open(QFile::WriteOnly | QFile::Text)) {
        QMessageBox::warning(this, tr("Application"),
                             tr("Cannot write file %1:\n%2.")
                             .arg(fileName)
                             .arg(file.errorString()));
        return false;
    }

    QTextStream out(&file);
    out << text[index]->toPlainText();
    setCurrentFile(fileName,index);
    statusBar()->showMessage(tr("File saved"), 2000);
    return true;
}//} Luu file text

//{ Set 1 file la file hien tai
void edagui::setCurrentFile(const QString &fileName,int index)
{
    curFile.append(fileName);
    text[index]->document()->setModified(false);
    text[index]->Setfilepath(fileName);
}//} Set 1 file la file hien tai

void edagui::createStatusBar()
{
    statusBar()->showMessage(tr("Ready"));
}

//Cac phuong thuc quan ly project va hien thi project
void edagui::on_treeView_doubleClicked(const QModelIndex &index)
{
    if(dir->isDir(index)) ui->treeView->expandsOnDoubleClick();
    else
    {
        QString filePath =  dir->fileInfo(index).absoluteFilePath();
        loadFile(filePath);
    }
}

void edagui::update_treeview(QString projectpath)
{
    QModelIndex index = dir->setRootPath(projectpath);
    rootproject = projectpath; //update duong dan project
    ui->treeView->setRootIndex(index);
    ui->treeView->expand(index);
    ui->treeView->scrollTo(index);
    ui->treeView->setCurrentIndex(index);
    ui->nameproject->setTitle(rootproject.rightRef(rootproject.length()-rootproject.lastIndexOf("/")-1).toString());
}

void edagui::on_actionNew_Project_triggered()
{
    CreateProject newproject;
    QString oldpath = newproject.projectpath = rootproject;
    newproject.setModal(true);
    newproject.exec();
    if(newproject.close() && oldpath != newproject.projectpath)
    {
        update_treeview(newproject.projectpath);
    }
}

void edagui::on_actionOpen_Project_triggered()
{
    QString oldpath = rootproject;
    QString path = QFileDialog::getExistingDirectory(this,"Open Project");
    if(oldpath != path && path != "")
    {
       update_treeview(path);
    }
    ui->treeView->expand(dir->index(rootproject+"/Design"));
}

//Cac phuong thuc chinh sua file text
void edagui::on_actionNew_File_triggered()
{
    QWidget *windows = new QWidget;
    QHBoxLayout *layout = new QHBoxLayout;
    CodeEditor *tabtext = new CodeEditor;
    tabtext->setupEditor();
    layout->addWidget(tabtext);
    windows->setLayout(layout);
    QString namedocument = "Text";
    ui->tabWidget->insertTab(ui->tabWidget->count(),windows,namedocument);
    ui->tabWidget->setCurrentWidget(windows);
    text.insert(text.size(),tabtext);
    createStatusBar();
    ui->actionSave->setEnabled(true);
    ui->actionCut->setEnabled(true);
    ui->actionCopy->setEnabled(true);
    ui->actionPaste->setEnabled(true);
    ui->actionSave_As->setEnabled(true);
}

void edagui::on_actionOpen_File_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(this);
    if (!fileName.isEmpty())
    {
        loadFile(fileName);
    }
}

void edagui::on_tabWidget_tabCloseRequested(int index)
{
    if (maybeSave(index))
    {
        QWidget *temp = ui->tabWidget->widget(index);
        ui->tabWidget->removeTab(index);
        delete temp;
        for (int i = 0; i < curFile.size(); ++i)
        {
            if (curFile[i]==text[index]->Getfilepath()) curFile.remove(i);
        }
        text.remove(index);
        if (text.size() == 0)
        {
            ui->actionSave->setDisabled(true);
            ui->actionCut->setDisabled(true);
            ui->actionCopy->setDisabled(true);
            ui->actionPaste->setDisabled(true);
            ui->actionSave_As->setDisabled(true);
        }
    }
}

bool edagui::on_actionSave_As_triggered()
{
    QFileDialog dialog(this,"Save as",rootproject);
    dialog.setWindowModality(Qt::WindowModal);
    dialog.setAcceptMode(QFileDialog::AcceptSave);
    QStringList files;
    if (dialog.exec())
        files = dialog.selectedFiles();
    else
        return false;
    return saveFile(files.at(0),ui->tabWidget->currentIndex());
}

bool edagui::on_actionSave_triggered()
{
    bool flag = false; //Kiem tra xem file duoc luu da ton tai chua
    QString currentNameText = text[ui->tabWidget->currentIndex()]->Getfilepath();
    for (int i = 0; i < curFile.size(); ++i)
    {
        if (currentNameText == curFile[i])
        {
            flag = true;
            return saveFile(curFile[i],ui->tabWidget->currentIndex());
        }
    }
    return on_actionSave_As_triggered();
}

void edagui::on_actionExit_triggered()
{
    for (int i = text.size()-1; i >= 0; --i)
    {
        ui->tabWidget->setCurrentWidget(ui->tabWidget->widget(i));
        if(maybeSave(i))
        {
            QWidget *temp = ui->tabWidget->widget(i);
            ui->tabWidget->removeTab(i);
            delete temp;
            text.remove(i);
        }
    }
    qApp->quit();
}

void edagui::on_actionCut_triggered()
{
    text[ui->tabWidget->currentIndex()]->cut();
}

void edagui::on_actionCopy_triggered()
{
    text[ui->tabWidget->currentIndex()]->copy();
}

void edagui::on_actionPaste_triggered()
{
    text[ui->tabWidget->currentIndex()]->paste();
}

void edagui::on_actionAbout_triggered()
{
    QMessageBox::information(this,"FCKBK","This program uses Qt version 5.4.1\nDevlopment by BKIC Laboratory");
}

//Cac phuong thuc giao tiep voi trinh bien dich FCKBK

//Bao lai dau ra tu EDA tool
void edagui::reportoutput()
{
    QByteArray strdata = edatool->readAllStandardOutput();
    ui->textEdit_command->setTextColor(Qt::black);
    ui->textEdit_command->append(strdata);
}

//Bao lai loi tu EDA tool
void edagui::reporterror()
{
    QByteArray strdata = edatool->readAllStandardError();
    ui->textEdit_command->setTextColor(Qt::red);
    ui->textEdit_command->append(strdata);
}

//Goi phan mem eda tool de bien dich va kiem chung
void edagui::on_actionRun_triggered()
{
    //Kiem tra xem da cai duong dan FCKBK tool chua
    if(edatoolpath == "") QMessageBox::warning(this,"Warning","Not Configuration FCKBK Tool");
    //Neu dang chay chuong trinh FCKBK tool se hien ra canh bao
    if(edatool->state() == QProcess::Running) QMessageBox::warning(this,"Warning","FCKBK have been running");
    else
    {
        ui->actionCommandBoolector->setEnabled(true);
        ui->actionCommandMiniSat->setEnabled(true);
        ui->actionCommandRead_RTL->setEnabled(true);
        ui->actionCommandVerify->setEnabled(true);
        ui->actionCommandStop->setEnabled(true);
        ui->actionRead_Property->setEnabled(true);
        ui->actionPrint_Circuit->setEnabled(true);
        ui->actionPrint_Property->setEnabled(true);
        ui->actionPrint_IPC->setEnabled(true);
        ui->textEdit_command->clear();
        QByteArray cmd;
        edatool->start(edatoolpath);
        connect(edatool,SIGNAL(readyReadStandardOutput()),this,SLOT(reportoutput()));
        connect(edatool,SIGNAL(readyReadStandardError()),this,SLOT(reporterror()));
        edatool->waitForStarted();
        cmd.append("read project - " + rootproject +"/");
        edatool->write(cmd+"\n");
        edatool->waitForBytesWritten();
    }
}

//Cai dat duong dan den eda tool
void edagui::on_actionEDA_Tool_triggered()
{
    edatoolpath = QFileDialog::getOpenFileName(this,"Open EDA Tool","/home","fck-bk-dbg");
}

//Hien thi menu khi an chuot phai o cua so quan ly project
void edagui::on_treeView_customContextMenuRequested(const QPoint &pos)
{
    // for most widgets
    QPoint globalPos = ui->treeView->mapToGlobal(pos);
    QModelIndex filename =  ui->treeView->indexAt(pos); //Tra ve index cua item duoc chon bang chuot phai
    QMenu myMenu;
    myMenu.addAction("Open");
    myMenu.addAction("Generate Run File");
    myMenu.addAction("Add New File");
    myMenu.addAction("Add Existing File");
    myMenu.addAction("Delete File");
    myMenu.addSeparator();
    myMenu.addAction("Wave Form Viewer");
    QAction* selectedItem = myMenu.exec(globalPos);
    if (selectedItem != NULL)
    {
        if (selectedItem->text() == "Open")
        {
            if(dir->isDir(filename)) ui->treeView->expand(filename);
            else
            {
                QString filePath =  dir->fileInfo(filename).absoluteFilePath();
                loadFile(filePath);
            }
        }
        else if (selectedItem->text() == "Generate Run File")   //Tu sinh ra file rtl.txt
        {
            QModelIndex dirDesign = dir->index(rootproject+"/Design");
            int numRows = dir->rowCount(dirDesign);
            //Sinh ra file rtl.txt
            QFile runfile(rootproject+"/Run/rtl.txt");
            runfile.open(QFile::WriteOnly);
            QTextStream out(&runfile);
            for (int row = 0; row < numRows; ++row)
            {
                QModelIndex childIndex = dir->index(row, 0, dirDesign);
                QString path = dir->data(childIndex).toString();
                //Kiem tra neu la file *.v thi in ra file rtl.txt
                if (path.rightRef(2).toString() == ".v") out << rootproject + "/Design/" + path + "\n";
            }
            runfile.close();
        }
        else if (selectedItem->text() == "Add New File")
        {
            if (dir->fileInfo(filename).isDir())
            {
                bool flagexist = false;
                QString newfile;
                //Hien thi hop thoai tao ten file va tao file
                addnewfile adddialog;
                adddialog.setModal(true);
                adddialog.exec();
                newfile = adddialog.getnamefile();
                if (adddialog.close())
                {
                    int numRows = dir->rowCount(filename);
                    for (int row = 0; row < numRows; ++row)
                    {
                        QModelIndex childIndex = dir->index(row, 0, filename);
                        QString namechild = dir->data(childIndex).toString();
                        namechild = namechild.rightRef(namechild.length()-namechild.lastIndexOf("/")-1).toString();
                        if (newfile == namechild) //Kiem tra xem file co ton tai
                        {
                            QMessageBox::warning(this,"File exist","File is exist");
                            flagexist = true;
                        }
                    }
                    if (!flagexist)
                    {
                        QFile file(dir->fileInfo(filename).absoluteFilePath()+"/"+newfile);
                        file.open(QFile::WriteOnly);
                        file.close();
                        ui->treeView->expand(filename);
                    }
                }
            }
        }
        else if (selectedItem->text() == "Add Existing File")   //code for add existing file
    {
        if (dir->fileInfo(filename).isDir())
        {
            QStringList AddFiles = QFileDialog::getOpenFileNames(this,"Add Existing File","/home");
            for (int i = 0; i < AddFiles.size(); ++i)
            {
                QString desFile = AddFiles.at(i).rightRef(AddFiles.at(i).length()-AddFiles.at(i).lastIndexOf("/")-1).toString();
                if (QFile::copy(AddFiles.at(i),dir->fileInfo(filename).absoluteFilePath()+"/"+desFile))
                {
                    ui->treeView->expand(filename);
                }
                else QMessageBox::warning(this,"warning","File existing, can not add file!");
            }
        }
    }
        else if(selectedItem->text() == "Delete File")          //code for delete file
        {
            if (dir->fileInfo(filename).isFile())
            {
                QMessageBox::StandardButton ret;
                ret = QMessageBox::warning(this, "Delete File","Are you delete file ?",
                                           QMessageBox::Yes | QMessageBox::No);
                if (ret == QMessageBox::Yes) QFile::remove(dir->fileInfo(filename).absoluteFilePath());
            }
        }
        else if (selectedItem->text() == "Wave Form Viewer")          //code for open by wave form
        {
            if (dir->fileInfo(filename).isFile()) on_actionWaveform_triggered();
        }
    }
}

//Tao ra file rtl.txt trong /Project/Run
void edagui::on_actionGenerate_Run_file_triggered()
{
    QModelIndex dirDesign = dir->index(rootproject+"/Design");
    int numRows = dir->rowCount(dirDesign);
    //Sinh ra file rtl.txt
    QFile runfile(rootproject+"/Run/rtl.txt");
    runfile.open(QFile::WriteOnly);
    QTextStream out(&runfile);
    for (int row = 0; row < numRows; ++row)
    {
        QModelIndex childIndex = dir->index(row, 0, dirDesign);
        QString path = dir->data(childIndex).toString();
        //Kiem tra neu la file *.v thi in ra file rtl.txt
        if (path.rightRef(2).toString() == ".v") out << rootproject + "/Design/" + path + "\n";
    }
    runfile.close();
}

//cac nut command thay cho viec viet lenh bang tay
//Gui lenh read rtl - Run/rtl.txt
void edagui::on_actionCommandRead_RTL_triggered()
{
    //Kiem tra xem da cai duong dan FCKBK chua
    if(edatoolpath == "") QMessageBox::warning(this,"Warning","FCKBK not start");
    else
    {
        QByteArray cmd;
        ui->textEdit_command->append("read rtl - Run/rtl.txt");
        cmd.append("read rtl - Run/rtl.txt");
        edatool->write(cmd+"\n");
        edatool->waitForBytesWritten();
    }
}

//Gui lenh read property
void edagui::on_actionRead_Property_triggered()
{
    //Kiem tra xem da cai duong dan FCKBK chua
    if(edatoolpath == "") QMessageBox::warning(this,"Warning","FCKBK not start");
    else
    {
        QString SVAfile = QFileDialog::getOpenFileName(this,"Add Existing File",rootproject+"/Property",tr("(*.sv)"));
        if (!SVAfile.isEmpty())
        {
            SVAfile = SVAfile.rightRef(SVAfile.length()-SVAfile.lastIndexOf("/")-1).toString();
            //Send read property SVA file to FCKBK Tool
            QByteArray cmd;
            ui->textEdit_command->append("read property - Property/"+SVAfile);
            cmd.append("read property - Property/"+SVAfile);
            edatool->write(cmd+"\n");
            edatool->waitForBytesWritten();
        }
        else
        {
            //Notify: No selected SVA file
        }

    }

}

//Gui lenh set solver = Btor
void edagui::on_actionCommandBoolector_triggered()
{
    //Kiem tra xem da cai duong dan FCKBK chua
    if(edatoolpath == "") QMessageBox::warning(this,"Warning","FCKBK not start");
    else
    {
        QByteArray cmd;
        cmd.append("set solver - Btor");
        ui->textEdit_command->append("set solver - Btor");
        edatool->write(cmd+"\n");
        edatool->waitForBytesWritten();
    }
}

//Gui lenh set solver = MiniSat
void edagui::on_actionCommandMiniSat_triggered()
{
    //Kiem tra xem da cai duong dan FCKBK chua
    if(edatoolpath == "") QMessageBox::warning(this,"Warning","FCKBK not start");
    else
    {
        QByteArray cmd;
        cmd.append("set solver - MiniSat");
        ui->textEdit_command->append("set solver - MiniSat");
        edatool->write(cmd+"\n");
        edatool->waitForBytesWritten();
    }
}

//Gui lenh verify
void edagui::on_actionCommandVerify_triggered()
{
    //Kiem tra xem da cai duong dan FCKBK chua
    if(edatoolpath == "") QMessageBox::warning(this,"Warning","FCKBK not start");
    else
    {
        QByteArray cmd;
        cmd.append("verify property");
        ui->textEdit_command->append("verify property");
        edatool->write(cmd+"\n");
        edatool->waitForBytesWritten();
    }
}

//Gui lenh exit
void edagui::on_actionCommandStop_triggered()
{
    //Kiem tra xem da cai duong dan FCKBK chua
    if(edatoolpath == "") QMessageBox::warning(this,"Warning","FCKBK not start");
    else
    {
        ui->actionCommandBoolector->setDisabled(true);
        ui->actionCommandMiniSat->setDisabled(true);
        ui->actionCommandRead_RTL->setDisabled(true);
        ui->actionCommandVerify->setDisabled(true);
        ui->actionCommandStop->setDisabled(true);
        ui->actionRead_Property->setDisabled(true);
        ui->actionPrint_Circuit->setDisabled(true);
        ui->actionPrint_Property->setDisabled(true);
        ui->actionPrint_IPC->setDisabled(true);
        QByteArray cmd;
        cmd.append("exit");
        ui->textEdit_command->clear();
        edatool->write(cmd+"\n");
        edatool->waitForBytesWritten();
    }
}

//Gui lenh print circuit
void edagui::on_actionPrint_Circuit_triggered()
{
    //Kiem tra xem da cai duong dan FCKBK chua
    if(edatoolpath == "") QMessageBox::warning(this,"Warning","FCKBK not start");
    else
    {
        QByteArray cmd;
        cmd.append("print circuit");
        ui->textEdit_command->append("print circuit");
        edatool->write(cmd+"\n");
        edatool->waitForBytesWritten();
    }
}

//Gui lenh print property
void edagui::on_actionPrint_Property_triggered()
{
    //Kiem tra xem da cai duong dan FCKBK chua
    if(edatoolpath == "") QMessageBox::warning(this,"Warning","FCKBK not start");
    else
    {
        QByteArray cmd;
        cmd.append("print property");
        ui->textEdit_command->append("print property");
        edatool->write(cmd+"\n");
        edatool->waitForBytesWritten();
    }
}

//Gui lenh print ipc
void edagui::on_actionPrint_IPC_triggered()
{
    //Kiem tra xem da cai duong dan FCKBK chua
    if(edatoolpath == "") QMessageBox::warning(this,"Warning","FCKBK not start");
    else
    {
        QByteArray cmd;
        cmd.append("print ipc");
        ui->textEdit_command->append("print ipc");
        edatool->write(cmd+"\n");
        edatool->waitForBytesWritten();
    }
}

void edagui::on_actionWaveform_triggered()
{    
    QStringList list;
    foreach(const QModelIndex &index, ui->treeView->selectionModel()->selectedIndexes())
    {
        list.append(dir->fileInfo(index).absoluteFilePath());
    }
    QString filePath = list.at(0);
    if (filePath.right(4) == ".vcd")
    {
        Dialog waveformsignal(filePath);
        if (waveformsignal.isActiveWindow() == true) return;
        waveformsignal.setModal(true);
        waveformsignal.resize(1200,600);
        waveformsignal.exec();
    }
    else QMessageBox::warning(this,"Not open by waveform","File is not VCD file");

}
