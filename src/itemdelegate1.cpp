#include "itemdelegate1.h"

ItemDelegate1::ItemDelegate1(QWidget *parent) :
    QItemDelegate(parent)
{
}

QSize ItemDelegate1::sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const
{

    return QSize(4, 24);
}

void ItemDelegate1::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{

    //----------- Get data as Qstring --------------------------------------//

    QString string1 = index.model()->data(index, Qt::DisplayRole).toString();
    int row = index.row();
    int column = index.column();

    //----------- Get previous data as Qstring------------------------------//
    QModelIndex index2 = index.sibling(row, column - 1);
    QString string2 = index2.model()->data(index2, Qt::DisplayRole).toString();

    // Convert to string

    std::string cstring1 = string1.toUtf8().constData();

    //Cursor time every cycle
    QPen pen3 = QPen(Qt::darkBlue);
    pen3.setWidth(1);
    painter->setPen(pen3);
    if (option.rect.x()>1)
    {
        painter->drawLine(option.rect.x()-1, option.rect.y(), option.rect.x()-1, option.rect.y() + 1000);
    }


    //-----------Đường viền bên phải----------------------------------------//
/* Hình dạng của 1 item
  ------------------------------->  (trục x) : độ rộng  là width
  |
  |
  | độ cao là height
  |
  |

  (trục y)

*/


//  Trường hợp 1: Dữ liệu dạng bus


    if ( string1.size() > 1 )
    {
        /*
        1.1: Dữ liệu hiện tại khác dữ liệu trước đấy.

        //-----------Vẽ đường chéo Polyline------------------------

            ______________________ PolyLine 1
        \  /
         \/
         /\
        /  \______________________ PolyLine 2
        */
        if ((string1 != string2) && (column > 1))
        {
            //----Tạo 3 điểm cho PolyLine 1----------//
            QPen pen1 = QPen(Qt::darkGreen);
            pen1.setWidth(1);

            const QPointF points1[3] = {
                    QPointF(option.rect.x() - 3, option.rect.y() + option.rect.height() - 2),
                    QPointF(option.rect.x() + 3, option.rect.y() + 3),
                    QPointF(option.rect.x() + option.rect.width() - 3, option.rect.y() + 3),
             };


            //----Tạo 3 điểm cho PolyLine 2----------//

            const QPointF points2[3] = {
                    QPointF(option.rect.x() - 3, option.rect.y() + 3),
                    QPointF(option.rect.x() + 3, option.rect.y() + option.rect.height() - 2),
                    QPointF(option.rect.x() + option.rect.width() - 3, option.rect.y() + option.rect.height() - 2),
            };

            painter->setPen(pen1);     // Tô màu cho đường vẽ
            painter->drawPolyline(points1, 3);  // Vẽ đường 1
            painter->drawPolyline(points2, 3);  // Vẽ đường 2

        }
        /*
        1.2: Dữ liệu hiện tại giống dữ liệu trước đấy.

        //-----------Vẽ 2 đường thẳng ------------------------

            ______________________ path1



            ______________________ path2
        */
        else
        {
            QPainterPath path1;
            QPainterPath path2;
            QPen pen1 = QPen(Qt::darkGreen);
            pen1.setWidth(1);

            path1.moveTo(option.rect.x()- 3, option.rect.y() + 3);     // điểm đầu đường 1
            path1.lineTo(option.rect.x()- 3 + option.rect.width(), option.rect.y() + 3); // điểm cuối đương 1
            painter->setPen(pen1);
            painter->drawPath(path1);


            path2.moveTo(option.rect.x() - 3, option.rect.y() + option.rect.height() - 2 );     // điểm đầu đường 2
            path2.lineTo(option.rect.x() - 3 + option.rect.width(), option.rect.y() + option.rect.height() - 2);  // điểm cuối đường 2
            painter->setPen(pen1);
            painter->drawPath(path2);
        }

        //----------Vẽ nội dung lên Bus -----------------//
            //-------Vẽ hình vuông ------------//
            /*
                   point1
                    ----------------- (x)
                    |               |
                    |               |
                    |               |
                    |               |
                    |               |
                    -----------------
                   (y)              point2
            */

            // Điểm góc trên bên trái của hình vuông

            QPointF point1 = QPointF(option.rect.x() + 8, option.rect.y());

            // Điểm góc dưới bên phải của hình vuông
            QPointF point2 = QPointF(option.rect.x() + option.rect.width(), option.rect.y() + option.rect.height() - 4);

            // Hình vuông
            QRectF rect1 = QRectF(point1,point2);
            // Option của Text
            QTextOption option1;

            //Chon mau chu hien thi
            QPen pentext = QPen(Qt::white);
            painter->setPen(pentext);

            // Căn lề trái và dưới
            option1.setAlignment(Qt::AlignLeft | Qt::AlignBottom );
            // Cho phép nội dung không bị bao lấy
            option1.setWrapMode(QTextOption::NoWrap);
            // Vẽ chữ

            QString binToHex="", tmp = "0000";
            while(string1.length()!=0)
            {
                    int len=string1.length();
                    tmp = string1.right(4);
                    string1.truncate(len-4);
                    if(!tmp.compare("0000") || !tmp.compare("000") || !tmp.compare("00") || !tmp.compare("0")) binToHex.prepend('0');
                    else if (!tmp.compare("0001") || !tmp.compare("001") || !tmp.compare("01") || !tmp.compare("1")) binToHex.prepend('1');
                    else if (!tmp.compare("0010") || !tmp.compare("010") || !tmp.compare("10")) binToHex.prepend('2');
                    else if (!tmp.compare("0011") || !tmp.compare("011") || !tmp.compare("11")) binToHex.prepend('3');
                    else if (!tmp.compare("0100") || !tmp.compare("100")) binToHex.prepend('4');
                    else if (!tmp.compare("0101") || !tmp.compare("101")) binToHex.prepend('5');
                    else if (!tmp.compare("0110") || !tmp.compare("110")) binToHex.prepend('6');
                    else if (!tmp.compare("0111") || !tmp.compare("111")) binToHex.prepend('7');
                    else if (!tmp.compare("1000")) binToHex.prepend('8');
                    else if (!tmp.compare("1001")) binToHex.prepend('9');
                    else if (!tmp.compare("1010")) binToHex.prepend('A');
                    else if (!tmp.compare("1011")) binToHex.prepend('B');
                    else if (!tmp.compare("1100")) binToHex.prepend('C');
                    else if (!tmp.compare("1101")) binToHex.prepend('D');
                    else if (!tmp.compare("1110")) binToHex.prepend('E');
                    else if (!tmp.compare("1111")) binToHex.prepend('F');
                    else binToHex.prepend('X');
            }
            painter->drawText(rect1, binToHex, option1);

    }

    /*
      Trường hợp 2: Dữ liệu dạng bit

    //-----------Vẽ đường Polygone------------------------

        2.1 0-1
                ______________________
               |
               |
               |
               |

        2.2 1-0

               |
               |
               |
               |______________________

        2.3 0-0




               _______________________


        2.4 1-1
               _______________________





    */
        else
        {
            switch (cstring1[0])
            {
                case '1' :
                {

                    // Trường hợp 1-1
                    /*
                      ___________________________


                      */
                    if ((string1 == string2) || (column==1))
                    {
                        QPainterPath path1;
                        QPen pen1 = QPen(Qt::green);
                        path1.moveTo(option.rect.x() - 1 , option.rect.y() + 3);     // điểm đầu đường 1
                        path1.lineTo(option.rect.x() - 1 + option.rect.width(), option.rect.y() + 3); // điểm cuối đương 1
                        pen1.setWidth(2);
                        painter->setPen(pen1);
                        painter->drawPath(path1);
                    }
                    // Trường hợp 0-1
                    /*

                            ______________________
                           /
                          /
                         /
                        /
                    */
                    else
                    {
                        QPainterPath path1;
                        QPainterPath path2;
                        QPen pen1 = QPen(Qt::green);
                        QPen pen2 = QPen(Qt::green);
                        //const QPointF points1[3];
                         //TH1:
                        if (string2=="x")
                        {
                            const QPointF points1[3] = {
                                QPointF(option.rect.x()-2, option.rect.y() + option.rect.height() - 12),
                                QPointF(option.rect.x() - 1 , option.rect.y() + 3),   //6 vs 3
                                QPointF(option.rect.x() - 1 + option.rect.width(), option.rect.y() + 3)};

                            //---Đường chéo---//
                            path1.moveTo(points1[0]);
                            path1.lineTo(points1[1]);
                            pen1.setWidth(1);
                            painter->setPen(pen1);
                            painter->drawPath(path1);
                            //---Đường thẳng----//
                            path2.moveTo(points1[1]);
                            path2.lineTo(points1[2]);
                            pen2.setWidth(2);
                            painter->setPen(pen2);
                            painter->drawPath(path2);
                        }
                        else
                        {

                            //----Tạo 3 điểm cho PolyLine 1----------//
                            const QPointF points1[3] = {
                                    QPointF(option.rect.x() - 1 , option.rect.y() + option.rect.height()-2),
                                    QPointF(option.rect.x()  , option.rect.y() + 3),
                                    QPointF(option.rect.x() - 1 + option.rect.width() , option.rect.y() + 3)};

                            //---Đường chéo---//
                            path1.moveTo(points1[0]);
                            path1.lineTo(points1[1]);
                            pen1.setWidth(1);
                            painter->setPen(pen1);
                            painter->drawPath(path1);
                            //---Đường thẳng----//
                            path2.moveTo(points1[1]);
                            path2.lineTo(points1[2]);
                            pen2.setWidth(2);
                            painter->setPen(pen2);
                            painter->drawPath(path2);
                        }
                    }
                }
                break;
                case '0':
                {

                // Trường hợp 0-0
                /*
                  _____________________________________

                  */
                    if ( (string1 == string2) || (column == 1) )
                    {
                        QPainterPath path2;
                        QPen pen2 = QPen(Qt::green);

                        path2.moveTo(option.rect.x() - 1, option.rect.y() + option.rect.height() - 2 );     // điểm đầu đường 2
                        path2.lineTo(option.rect.x() - 1 + option.rect.width(), option.rect.y() + option.rect.height() - 2);  // điểm cuối đường 2
                        pen2.setWidth(1);
                        painter->setPen(pen2);
                        painter->drawPath(path2);

    //
                    }
                // Trường hợp 1-0

                /*

                    \
                     \
                      \
                       \______________________

               */
                //----Tạo 3 điểm cho PolyLine 2----------//
                    else
                    {
                        QPainterPath path1;
                        //QPainterPath path2;
                        QPen pen1 = QPen(Qt::green);
                        //QPen pen2 = QPen(Qt::red);
                        if (string2=="1")
                        {
                            const QPointF points2[3] = {
                                QPointF(option.rect.x() - 1, option.rect.y() + 3),
                                QPointF(option.rect.x() - 1 , option.rect.y() + option.rect.height() - 2), //6 vs 2
                                QPointF(option.rect.x() - 1 + option.rect.width(), option.rect.y() + option.rect.height() - 2)};

                            //---Đường chéo---//
                            path1.moveTo(points2[0]);
                            path1.lineTo(points2[1]);
                            pen1.setWidth(1);
                            painter->setPen(pen1);
                            painter->drawPath(path1);

                            //---Đường thẳng----//
                            path1.moveTo(points2[1]);
                            path1.lineTo(points2[2]);
                            pen1.setWidth(1);
                            painter->setPen(pen1);
                            painter->drawPath(path1);
                        }
                        else
                        {
                            const QPointF points2[3] = {
                                    QPointF(option.rect.x() - 1, option.rect.y()+12),
                                    QPointF(option.rect.x() - 1 , option.rect.y() + option.rect.height() - 2), //6 vs 2
                                    QPointF(option.rect.x() - 1 + option.rect.width(), option.rect.y() + option.rect.height() - 2)};

                            //---Đường chéo---//
                            path1.moveTo(points2[0]);
                            path1.lineTo(points2[1]);
                            pen1.setWidth(1);
                            painter->setPen(pen1);
                            painter->drawPath(path1);

                            //---Đường thẳng----//
                            path1.moveTo(points2[1]);
                            path1.lineTo(points2[2]);
                            pen1.setWidth(1);
                            painter->setPen(pen1);
                            painter->drawPath(path1);
                        }
                    }
                }
                break;
                case 'x':
                {
                    // Trường hợp x-x
                    /*
                      _____________________________________

                      */
                    if ( (string1 == string2) || (column == 1) )
                    {
                        QPainterPath path2;
                        QPen pen2 = QPen(Qt::red);
                        path2.moveTo(option.rect.x() - 1, option.rect.y() + option.rect.height() - 12 );     // điểm đầu đường 2
                        path2.lineTo(option.rect.x() - 1 + option.rect.width(), option.rect.y() + option.rect.height() - 12);  // điểm cuối đường 2
                        pen2.setWidth(1);
                        painter->setPen(pen2);
                        painter->drawPath(path2);
                    }
                            // Trường hợp 1-x

                            /*

                                 |
                                 |
                                 |
                                 |______________________

                           */
                            //----Tạo 3 điểm cho PolyLine 2----------//
                    else
                    {
                        QPainterPath path1;
                        //QPainterPath path2;
                        QPen pen1 = QPen(Qt::red);
                        //QPen pen2 = QPen(Qt::red);
                        if (string2=="1")
                        {
                            const QPointF points2[3] = {
                            QPointF(option.rect.x() -1 , option.rect.y() + 3),
                            QPointF(option.rect.x() - 1 + 0, option.rect.y() + option.rect.height() - 12), //6 vs 2
                            QPointF(option.rect.x() - 1 + option.rect.width(), option.rect.y() -10+ option.rect.height() - 2)};
                            //---Đường chéo---//
                            path1.moveTo(points2[0]);
                            path1.lineTo(points2[1]);
                            pen1.setWidth(1);
                            painter->setPen(pen1);
                            painter->drawPath(path1);
                            //---Đường thẳng----//
                            path1.moveTo(points2[1]);
                            path1.lineTo(points2[2]);
                            pen1.setWidth(1);
                            painter->setPen(pen1);
                            painter->drawPath(path1);
                        }
                        else
                        {
                            const QPointF points2[3] = {
                                QPointF(option.rect.x() - 1, option.rect.y()-3+option.rect.height() ),
                                QPointF(option.rect.x() - 1 + 0, option.rect.y()+ option.rect.height() - 12), //6 vs 2
                                QPointF(option.rect.x() - 1 + option.rect.width(), option.rect.y()-10 + option.rect.height() -2)};

                            //---Đường chéo---//
                            path1.moveTo(points2[0]);
                            path1.lineTo(points2[1]);
                            pen1.setWidth(1);
                            painter->setPen(pen1);
                            painter->drawPath(path1);


                            //---Đường thẳng----//
                            path1.moveTo(points2[1]);
                            path1.lineTo(points2[2]);
                            pen1.setWidth(1);
                            painter->setPen(pen1);
                            painter->drawPath(path1);
                        }
                    }
                }
                break;
                default :
                {
                    QPainterPath path3;
                    QPen pen3 = QPen(Qt::black);
                    path3.moveTo(option.rect.x(), option.rect.y() + option.rect.height() / 2);
                    path3.lineTo(option.rect.x() + option.rect.height(), option.rect.y() + option.rect.height() / 2);
                    pen3.setWidth(1);
                    painter->setPen(pen3);
                    painter->drawPath(path3);

                    break;
                }
            }
        }

    }
