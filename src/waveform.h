#ifndef WAVEFORM_H
#define WAVEFORM_H
#include <QDialog>
#include <QtCore>
#include <QtGui>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QDataWidgetMapper>
#include <QTabWidget>
#include <QFileDialog>
#include <QLabel>
#include <QGroupBox>
#include <QTreeView>
#include <QListView>
#include <QDirModel>
#include <QTableView>
#include <QSpinBox>
#include <QtWidgets>
#include <QTextEdit>
#include <QPushButton>
#include <QPainter>
#include <QHeaderView>
#include <QPaintEvent>
#include <QWidget>
#include <QMainWindow>
#include <QPoint>
#include <QMenu>
#include <QAction>
#include <QSortFilterProxyModel>

#include <itemdelegate.h>
#include <itemdelegate1.h>
#include <itemdelegate2.h>
#include <fsmproxy.h>

//-----------Plugins-----------//
#include "minisplitter.h"
#include "manhattanstyle.h"
#include "styledbar.h"
#include "fancytabwidget.h"
#include "vcd.h"
#include "mytreeview.h"

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QString _pathVcdfile, QWidget *parent = 0);
    ~Dialog();

private:
    Ui::Dialog *ui;
    //------------Cac ham khoi tao------------//
    void createActions();
    void createToolBars();
    void setupModel(QStandardItemModel *model, QString pathVcdFile);    //Hàm khởi tạo Model
    void createTreeView();              // Hàm tạo Nhóm Tree View
    //------------Cac ham khoi tao------------//


    QHBoxLayout *mainlayout;            //layout de setup mainwindow vao Dialog
    QMainWindow *mainwindow;            //control chinh chua tat ca cac list signal va cac treeview
    QString pathVCDfile;                //Duong dan cua file VCD lay tu edagui

    VCD *vcdfile;                       //Luu tru du lieu doc tu file VCD
    int numberSignalinWaveform;         //So cac tin hieu co trong waveform

    ///1---------------Toolbar---------------//
    QToolBar *resizeToolbar;
    QToolBar *editToolbar;
    QToolBar *timeToolbar;
    QToolBar *currentCursor;
    QLabel *timeFromLabel;
    QLabel *timeToLabel;
    QLabel *signalsLabel;
    QLabel *timeScaleLabel;
    QLabel *cursorLabel;
    QLineEdit *timeFrom;
    QLineEdit *timeTo;
    ///1---------------Toolbar---------------//

    ///2---------------Actions---------------//
    QAction *openAct;
    QAction *zoomInAct;
    QAction *zoomOutAct;
    QAction *zoomFitAct;
    ///2---------------Actions---------------//

    ///3-------------WaveForm----------------//
    int column_g;
    ///3-------------WaveForm----------------//


    ///4------------Nhóm Waveform Viewer----------//
    QStandardItemModel *model;                  //model chua cac du lieu de ve tin hieu tren waveform

    QPalette *palette;
    QTreeView *treeView;                //Dung de hien thi label cac tin hieu duoc ve
    QTreeView *treeView_2;              //Dung de hien thi gia tri cac tin hieu duoc chon o wave form
    myTreeView *treeView_3;             //Dung de hien thi waveform
    ///4------------Nhóm Waveform Viewer----------//


    ///5----Nhom Display Signal Selection---------//
    QGroupBox *displaysignal;
    QVBoxLayout *layoutdisplay;
    QHBoxLayout *layoutbutton;
    QHBoxLayout *layoutfilter;
    QLabel *labelfilter;
    QListView *listsignal;
    QPushButton *add;
    QPushButton *insert;
    QPushButton *remove;
    QStringListModel *QSLMSignal;
    QStringList listSignalDisplay;  //Bien luu tru label cac signal
    QAbstractItemModel *modelSignal;
    ///5----Nhom Display Signal Selection---------//

    FSMProxy *proxy;

    ///7--------------Delegate--------------------//
    ItemDelegate *delegate;
    ItemDelegate1 *delegate1;
    ItemDelegate2 *delegate2;
    ///7--------------Delegate--------------------//

    ///8--------------Menu Bar--------------------//
    QMenuBar *menuBar;
    ///8--------------Menu Bar--------------------//

    //--------- Nhóm Plugins------------//
    Manhattan::MiniSplitter *splitter1;

private slots:
    void updateTreeView();      // update Tree View 2
    void zoomInTreeView();      //Zoom In
    void zoomOutTreeView();     //Zoom Out
    void zoomFitTreeView();     //Zoom Fit
    void updateCursor();        //update vi tri thanh gat
    void buttonAddAction();     //Add Button
    void buttonInsertAction();  //Insert Button
    void buttonRemoveAction();  //Remove Button
    void open();                // Open file
    void treeView_2_ContextMenu(const QPoint &);     //Hien thi menu khi an chuot phai o cua so treeview_2
};

#endif // WAVEFORM_H
