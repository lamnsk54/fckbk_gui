#-------------------------------------------------
#
# Project created by QtCreator 2015-07-17T14:45:26
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = FCKBKGUI_v3
TEMPLATE = app

DEPENDPATH += plugins
INCLUDEPATH += plugins
LIBS+=  -Lplugins -lqt-manhattan-style

SOURCES += \
    addnewfile.cpp \
    createproject.cpp \
    edagui.cpp \
    highlight.cpp \
    keyenterreceiver.cpp \
    main.cpp \
    codeeditor.cpp \
    mytreeview.cpp \
    myqtextedit.cpp \
    signal.cpp \
    vcd.cpp \
    fsmproxy.cpp \
    itemdelegate.cpp \
    itemdelegate2.cpp \
    waveform.cpp

HEADERS  += \
    addnewfile.h \
    createproject.h \
    edagui.h \
    highlight.h \
    keyenterreceiver.h \
    codeeditor.h \
    mytreeview.h \
    myqtextedit.h \
    signal.h \
    vcd.h \
    fsmproxy.h \
    itemdelegate.h \
    itemdelegate2.h \
    waveform.h

FORMS    += \
    addnewfile.ui \
    createproject.ui \
    edagui.ui \
    dialog.ui

RESOURCES += \
    icon.qrc \
    dialog.qrc

DISTFILES +=
