#ifndef EDAGUI_H
#define EDAGUI_H
/*********************************************************************************
 * Class chua giao dien chinh cua FCKBK tool, bao gom 3 thanh phan chinh:
 *      - Cua so quan ly project
 *      - Cua so soan thao
 *      - Cua so command
 * Class chua toan bo cac khoi tao ban dau cho FCKBKGUI va tat ca cac phuong thuc
 * lien quan den 3 thanh phan tren
 *********************************************************************************/
#include <QMainWindow>
#include <QVector>
#include <QQueue>
#include <QTextEdit>
#include <QMessageBox>
#include <QFile>
#include <QFileDialog>
#include <QTextStream>
#include <QFileSystemModel>
#include <QProcess>
#include <QMenu>
#include <QPoint>
#include <QModelIndex>
#include <QDebug>

#include "createproject.h"
#include "myqtextedit.h"
#include "highlight.h"
#include "keyenterreceiver.h"
#include "addnewfile.h"
#include "codeeditor.h"
#include "waveform.h"

namespace Ui {
class edagui;
}

class edagui : public QMainWindow
{
    Q_OBJECT

public:
    explicit edagui(QWidget *parent = 0);
    ~edagui();
    QString rootproject; //Bien nay de truyen duong dan cua project cho EDA Tool
    //Cac thao tac chinh sua file text
private slots:
    void on_actionNew_File_triggered();     //Tao file moi

    void on_actionOpen_File_triggered();    //Mo mot file co san

    void on_tabWidget_tabCloseRequested(int index);     //Dong 1 cua so soan thao

    bool on_actionSave_As_triggered();      //Save mot file dang mo

    bool on_actionSave_triggered();         //Save mot file dang mo

    void on_actionExit_triggered();         //Thoat chuong trinh

    void on_actionCut_triggered();          //Cut

    void on_actionCopy_triggered();         //Copy

    void on_actionPaste_triggered();        //Paste

    //Cac phuong thuc giup giao tiep voi EDA tool
private slots:
    void reportoutput();    //bao lai dau ra tu edatool

    void reporterror();     //bao lai loi tu edatool

    void on_actionRun_triggered();  //start FCKBK

    void on_actionEDA_Tool_triggered(); //Cau hinh duong dan cho FCKBK tool, duong dan den file fck-bk-dbg

    //Cac phuong thuc quan ly project va hien thi project
    void on_treeView_doubleClicked(const QModelIndex &index);   //Mo file/folder khi double click vao cai item

    void on_actionNew_Project_triggered();      //Tao mot project moi

    void update_treeview(QString projectpath);  //Cap nhat cua so quan ly project khi co thay doi

    void on_actionOpen_Project_triggered();     //Mo mot project co san

    //Hien thi menu khi an chuot phai o cua so quan ly project
    void on_treeView_customContextMenuRequested(const QPoint &pos);

    //Cac phuong thuc giao tiep voi trinh bien dich FCKBK
    void on_actionGenerate_Run_file_triggered();        //Tao ra file rtl.txt trong /Project/Run

    void on_actionCommandRead_RTL_triggered();          //Gui lenh read rtl - Run/rtl.txt

    void on_actionCommandVerify_triggered();            //Gui lenh verify

    void on_actionCommandBoolector_triggered();         //Gui lenh set solver = Btor

    void on_actionCommandMiniSat_triggered();           //Gui lenh set solver = MiniSat

    void on_actionCommandStop_triggered();              //Gui lenh exit

    void on_actionRead_Property_triggered();            //Gui lenh read property

    void on_actionPrint_Circuit_triggered();            //Gui lenh print circuit

    void on_actionPrint_Property_triggered();           //Gui lenh print property

    void on_actionPrint_IPC_triggered();                //Gui lenh print ipc

    void on_actionAbout_triggered();

    void on_actionWaveform_triggered();

private:
    bool maybeSave(int index);  //Kiem tra va dua ra lua chon co muon save cua so soan thao hien tai khong
    void loadFile(const QString &fileName); //Mo mot file text
    bool saveFile(const QString &fileName, int index);  //Luu file text
    void setCurrentFile(const QString &fileName, int index);    //Set 1 file la file hien tai
    /*
     * file hien tai la file da co trong he thong (da duoc luu va co duong dan)
     * co nhiem vu quan ly cac cua so soan thao dang duoc mo, nhung truong hop duoc setCurrentFile() :
     *  - Nhung file ton tai trong o dia va duoc mo
     *  - Nhung file moi duoc tao, va da duoc save as
     */
    void createStatusBar();
    QVector <QString> curFile;  //Chua ten nhung file dang duoc mo (da ton tai trong he thong)

    //Khai bao bien
private:
    Ui::edagui *ui;
    QVector <CodeEditor*> text;  //Bien quan ly cac tab duoc mo ra
    QFileSystemModel *dir;  //Quan ly thu muc ma treeview dang duyet
    QProcess *edatool;  //tro toi chuong trinh eda tool trong luc chay
    QString edatoolpath;    //Bien nay chua duong dan den edatool
    keyEnterReceiver *key;
    QString pathOfVcdFile;
};

#endif // EDAGUI_H
