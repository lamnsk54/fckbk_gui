#include "keyenterreceiver.h"

keyEnterReceiver::keyEnterReceiver(QTextEdit *text1,QProcess *proc1)
{
    text = new QTextEdit;
    proc = new QProcess;
    text = text1;
    proc = proc1;
}
keyEnterReceiver::~keyEnterReceiver()
{
    delete text;
    delete proc;
}

bool keyEnterReceiver::eventFilter(QObject *obj, QEvent *event)
{
    if(event->type() == QEvent::KeyPress)
    {
        QKeyEvent *key = static_cast<QKeyEvent*>(event);
        if(key->key()==Qt::Key_Enter || key->key()==Qt::Key_Return)
        {
            QString context;
            context = text->document()->lastBlock().text();
            while(context.at(0) == '>') context.remove(0,1);    //loai bo dau '>'
            while(context.at(0) == ' ') context.remove(0,1);    //loai bo dau ' '
            QByteArray cmd;
            cmd.append(context);
            proc->write(cmd+"\n");
            proc->waitForBytesWritten();
        }
        else
        {
            return QObject::eventFilter(obj, event);
        }
        return true;
    }
    else
    {
        return QObject::eventFilter(obj, event);
    }

    return false;
}
