#ifndef CREATEPROJECT_H
#define CREATEPROJECT_H
/*
 * Class co form, co chuc nang tao mot project FCKBK moi, form co chuc nang
 *  nhap ten va duong dan cho project
 */
#include <QDialog>
#include <QFileDialog>
#include <QDir>
#include <QMessageBox>
#include "edagui.h"
namespace Ui {
class CreateProject;
}
class CreateProject : public QDialog
{
    Q_OBJECT

public:
    explicit CreateProject(QWidget *parent = 0);
    ~CreateProject();
    QString projectpath;

private slots:
    //Browse den vi tri muon tao project
    void on_Browse_clicked();
    //Huy khong tao project
    void on_Cancel_clicked();
    //Hoan thanh viec tao project
    void on_Finish_clicked();


private:
    Ui::CreateProject *newpro;
};

#endif // CREATEPROJECT_H
