#ifndef ADDNEWFILE_H
#define ADDNEWFILE_H
/*Class co form, co nhiem vu hien thi hop thoai de nhap ten
 * file moi de add vao thu muc trong project cua FCKBK
 */

#include "edagui.h"

namespace Ui {
class addnewfile;
}

class addnewfile : public QDialog
{
    Q_OBJECT

public:
    explicit addnewfile(QWidget *parent = 0);
    ~addnewfile();
    void setnamefile(QString _namefile);
    QString getnamefile();


private slots:
    void on_Cancel_clicked();   //phuong thuc huy khong tao file moi

    void on_Create_clicked();   //phuong thuc tao file moi

private:
    Ui::addnewfile *addnew; //Con tro tro vao hop thoai add new file
    QString namefile;   //Bien luu tru ten cua file duoc nhap vao o hop thoai
};

#endif // ADDNEWFILE_H
