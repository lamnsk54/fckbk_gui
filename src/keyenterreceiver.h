#ifndef KEYENTERRECEIVER_H
#define KEYENTERRECEIVER_H

#include <QObject>
#include <QEvent>
#include <QKeyEvent>
#include <QTextEdit>
#include <QProcess>
#include <QTextBlock>
/*
 * Class nay de them su kien an enter cho cua so lenh command
 *  Khi an nut enter, khoi van ban moi duoc ghi vao cua so text_command
 *  se duoc gui den FCKBK, class se tu loai bo ki tu '>' va ' '
 */
class keyEnterReceiver : public QObject
{
    Q_OBJECT
public:
    keyEnterReceiver(QTextEdit *text1,QProcess *proc1);
    ~keyEnterReceiver();
protected:
    bool eventFilter(QObject *obj, QEvent *event);
private:
    QTextEdit *text;
    QProcess *proc;
};
#endif // KEYENTERRECEIVER_H
