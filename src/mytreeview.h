#ifndef MYTREEVIEW_H
#define MYTREEVIEW_H

#include <QTreeView>
#include <QPaintEvent>
#include <QPainter>
#include <QHeaderView>
#include <QMessageBox>

class myTreeView: public QTreeView
{
public:
    myTreeView();
    ~myTreeView();
    void setDrawColumn(int _column);
    int getDrawColumn();
protected:
    void paintEvent(QPaintEvent *event);
private:
    int column;

};

#endif // MYTREEVIEW_H
