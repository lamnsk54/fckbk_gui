#include "waveform.h"
#include "ui_dialog.h"

Dialog::Dialog(QString _pathVcdfile, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
    mainwindow = new QMainWindow;
    QSLMSignal = new QStringListModel;
    mainlayout = new QHBoxLayout;
    numberSignalinWaveform = 0;
    vcdfile = new VCD;
    pathVCDfile = _pathVcdfile;
    //-------Setup-------------//
    createActions();
    createToolBars();
    createTreeView();
    mainwindow->setCentralWidget(splitter1);
    mainlayout->addWidget(mainwindow);
    setLayout(mainlayout);
    //-------------Create Graphics---------//
    setWindowTitle("Waveform Viewer");
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::open()
{
    int totalTime = vcdfile->getCycle() + 1;
    delete vcdfile;
    for(int i = 0; i < numberSignalinWaveform; i++)
    {
        if (numberSignalinWaveform != 0) model->takeRow(0);
        numberSignalinWaveform--;
    }
    if (numberSignalinWaveform == 0)
    {
        QList<QStandardItem *> bus_i;                                   // Bus
        int bitWidth = 1;
        //{--------------------------Name Bus---------------------------//
        QStandardItem *bus_name = new QStandardItem();                  // Ten Bus
        QString bus_name_s = "";
        bus_name->setIcon(QIcon(":/images/Im-Yahoo-48.png"));
        bus_name->setText(bus_name_s);
        bus_name->setTextAlignment(Qt::AlignRight);
        bus_name->setEditable(false);
        bus_name->setToolTip(bus_name_s);
        QString bit_name_s;
        QStandardItem *bit_name = new QStandardItem();   // Hang Bit;
        bit_name_s = "";
        bit_name->setText(bit_name_s);
        bit_name->setTextAlignment(Qt::AlignRight);
        bit_name->setEditable(false);
        bit_name->setToolTip(bit_name_s);
        bus_i.append(bit_name);

        for (int c = 1; c < totalTime; c++)
        {
            QStandardItem *bus_value = new QStandardItem();             // Gia tri Bus
            QString busValue = "";
            bus_value->setText(busValue);
            bus_value->setTextAlignment(Qt::AlignRight);
            bus_value->setEditable(false);
            bus_value->setToolTip(busValue);
            bus_i.append(bus_value);
            for (int b = 0; b < bitWidth; ++b)
            {
                QStandardItem *bit_value = new QStandardItem();         // Hang Bit;
                QChar bitc;
                bitc = busValue[b];
                QString bit_value_s = QString(bitc);
                bit_value->setText(bit_value_s);
                bit_value->setTextAlignment(Qt::AlignRight);
                bit_value->setEditable(false);
                bit_value->setToolTip(bit_value_s);
                bus_name->setChild(b,c,bit_value);
            }
        }
        model->insertRow(0,bus_i);      // Add Bus
    }
    QString filename =  QFileDialog::getOpenFileName( this,
                                  tr("Open file"),
                                  QDir::currentPath(),
                                  tr("VCD files (*.vcd);;All files (*.*)"),
                                  0,QFileDialog::DontUseNativeDialog);
    vcdfile = new VCD;
    qDebug() <<filename;
    setupModel(model,filename);
}

void Dialog::setupModel(QStandardItemModel *model, QString pathVcdFile)
{
    vcdfile->ReadVCD(pathVcdFile);
    model->setRowCount(0);
    model->setColumnCount(0);
    int numberSignal = vcdfile->numberOfSignal();
    int totalTime = vcdfile->getCycle() + 1;
    for (int i = 0; i < numberSignal; i++)
        if (vcdfile->getSignal(i)->getBitWidth() > 1)
            listSignalDisplay.append(vcdfile->getSignal(i)->getLabel() +
                                     tr(" [%0:0]").arg(vcdfile->getSignal(i)->getBitWidth()-1));
        else listSignalDisplay.append(vcdfile->getSignal(i)->getLabel());
    QSLMSignal->setStringList(listSignalDisplay);
    listsignal->setModel(QSLMSignal);
    listsignal->setEditTriggers(QAbstractItemView::NoEditTriggers);
    listsignal->setSelectionMode(QAbstractItemView::ExtendedSelection);

    QList<QStandardItem *> bus_i;                                   // Bus
    int bitWidth = 1;
    //{--------------------------Name Bus---------------------------//
    QStandardItem *bus_name = new QStandardItem();                  // Ten Bus
    QString bus_name_s = "";
    bus_name->setIcon(QIcon(":/images/Im-Yahoo-48.png"));
    bus_name->setText(bus_name_s);
    bus_name->setTextAlignment(Qt::AlignRight);
    bus_name->setEditable(false);
    bus_name->setToolTip(bus_name_s);
    QString bit_name_s;
    QStandardItem *bit_name = new QStandardItem();   // Hang Bit;
    bit_name_s = "";
    bit_name->setText(bit_name_s);
    bit_name->setTextAlignment(Qt::AlignRight);
    bit_name->setEditable(false);
    bit_name->setToolTip(bit_name_s);
    bus_i.append(bit_name);

    for (int c = 1; c < totalTime; c++)
    {
        QStandardItem *bus_value = new QStandardItem();             // Gia tri Bus
        QString busValue = "";
        bus_value->setText(busValue);
        bus_value->setTextAlignment(Qt::AlignRight);
        bus_value->setEditable(false);
        bus_value->setToolTip(busValue);
        bus_i.append(bus_value);
        for (int b = 0; b < bitWidth; ++b)
        {
            QStandardItem *bit_value = new QStandardItem();         // Hang Bit;
            QChar bitc;
            bitc = busValue[b];
            QString bit_value_s = QString(bitc);
            bit_value->setText(bit_value_s);
            bit_value->setTextAlignment(Qt::AlignRight);
            bit_value->setEditable(false);
            bit_value->setToolTip(bit_value_s);
            bus_name->setChild(b,c,bit_value);
        }
    }
    model->insertRow(0,bus_i);      // Add Bus
}

void Dialog::createTreeView()
{
    //---------------Setup------------------//
    model = new QStandardItemModel(10,10);
    palette = new QPalette();

    //---Constructor display list signal----//
    layoutdisplay = new QVBoxLayout;
    layoutbutton = new QHBoxLayout;
    layoutfilter = new QHBoxLayout;
    add = new QPushButton;
    insert = new QPushButton;
    remove = new QPushButton;
    listsignal = new QListView;
    labelfilter = new QLabel;

    proxy = new FSMProxy();
    treeView = new QTreeView();
    treeView_2 = new QTreeView();
    treeView_3 = new myTreeView();
    treeView_2->setContextMenuPolicy(Qt::CustomContextMenu);

    setupModel(model,pathVCDfile);

    timeFrom->setText("0 "+ vcdfile->getTimeUnit());
    timeTo->setText(QString(vcdfile->getCycle() + 47) + tr(" ") + vcdfile->getTimeUnit());
    timeScaleLabel->setText(tr("Time scale: %0 %1").arg(vcdfile->getTimeScale()).arg(vcdfile->getTimeUnit()));
    cursorLabel->setText(tr("Cursor: 0 %0").arg(vcdfile->getTimeUnit()));

    //-------Set Model------------//
    treeView->setModel(model);
    treeView_2->setModel(model);
    treeView_3->setModel(model);

    //-------Background------------//
    palette->setColor(QPalette::Base, Qt::black);
    palette->setColor(QPalette::Text, Qt::white);
    treeView->setPalette(*palette);
    treeView_2->setPalette(*palette);
    treeView_3->setPalette(*palette);

    //-------Selection------------//
    treeView->setSelectionModel(treeView_2->selectionModel());
    treeView->setSelectionMode(QAbstractItemView::ContiguousSelection);
    treeView_2->setSelectionMode(QAbstractItemView::ContiguousSelection);
    treeView_3->setSelectionMode(QAbstractItemView::ContiguousSelection);
    //-------Hide Column----------//
    column_g = vcdfile->getCycle() + 1;
    for (int i = 0; i < column_g; i++)
    {
        if (i != 0)                   // Ẩn tree 1, trừ cột 0
        {
            treeView->hideColumn(i);
        }
        if (i != 1)                   // Ẩn tree 2, trừ cột 1
        {
            treeView_2->hideColumn(i);
        }
    }
    treeView_3->hideColumn(0);      // Ẩn tree 3 cột 0
    //----------Set Header--------//
    // Set Header cột 0
    QStandardItem *label1 = new QStandardItem(QString("Signals"));
    model->setHorizontalHeaderItem(0,label1);

    // Set Header các cột còn lại
    for (int i = 1; i < column_g; i++)
    {
        QStandardItem *label_i = new QStandardItem(QString("#%0").arg(i-1));
        model->setHorizontalHeaderItem(i, label_i);
    }

    //------------Scroll----------//
    mainwindow->connect(treeView->verticalScrollBar(),SIGNAL(valueChanged(int)),treeView_2->verticalScrollBar(),SLOT(setValue(int)));
    mainwindow->connect(treeView_2->verticalScrollBar(),SIGNAL(valueChanged(int)),treeView->verticalScrollBar(),SLOT(setValue(int)));
    mainwindow->connect(treeView->verticalScrollBar(),SIGNAL(valueChanged(int)),treeView_3->verticalScrollBar(),SLOT(setValue(int)));
    mainwindow->connect(treeView_3->verticalScrollBar(),SIGNAL(valueChanged(int)),treeView->verticalScrollBar(),SLOT(setValue(int)));

    //----------Expand------------//
    mainwindow->connect(treeView,SIGNAL(expanded(QModelIndex)),treeView_2,SLOT(expand(QModelIndex)));
    mainwindow->connect(treeView_2,SIGNAL(expanded(QModelIndex)),treeView_3,SLOT(expand(QModelIndex)));
    mainwindow->connect(treeView_3,SIGNAL(expanded(QModelIndex)),treeView,SLOT(expand(QModelIndex)));

    //----------Collapse---------//
    mainwindow->connect(treeView,SIGNAL(collapsed(QModelIndex)),treeView_2,SLOT(collapse(QModelIndex)));
    mainwindow->connect(treeView_2,SIGNAL(collapsed(QModelIndex)),treeView_3,SLOT(collapse(QModelIndex)));
    mainwindow->connect(treeView_3,SIGNAL(collapsed(QModelIndex)),treeView,SLOT(collapse(QModelIndex)));

    //----------Update Cursor-------//
    mainwindow->connect(treeView_3->selectionModel(),SIGNAL(currentColumnChanged(QModelIndex,QModelIndex)),this,SLOT(updateCursor()));

    //----------Update TreeView 2------//
    mainwindow->connect(treeView_3->selectionModel(),SIGNAL(currentColumnChanged(QModelIndex,QModelIndex)),this,SLOT(updateTreeView()));

    //----------Update TreeView 3------//
    mainwindow->connect(zoomInAct,SIGNAL(triggered()),this, SLOT(zoomInTreeView()));
    mainwindow->connect(zoomOutAct,SIGNAL(triggered()),this, SLOT(zoomOutTreeView()));
    mainwindow->connect(zoomFitAct, SIGNAL(triggered()), this, SLOT(zoomFitTreeView()));

    //-----------Action Button---------//
    mainwindow->connect(add,SIGNAL(clicked()),this, SLOT(buttonAddAction()));
    mainwindow->connect(insert,SIGNAL(clicked()),this, SLOT(buttonInsertAction()));
    mainwindow->connect(remove,SIGNAL(clicked()),this, SLOT(buttonRemoveAction()));

    //-----------Open menu on treeview_2---------//
    mainwindow->connect(treeView_2,SIGNAL(customContextMenuRequested(const QPoint &)),this,SLOT(treeView_2_ContextMenu(const QPoint &)));

    //----------Size Tree View----------------//
    treeView_2->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    treeView->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    treeView_3->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

    //----------Filter Model --------------//
    treeView->setMinimumWidth(6);
    treeView_2->setMinimumWidth(6);
    treeView_3->setMinimumWidth(6);

    delegate = new ItemDelegate();
    delegate1 = new ItemDelegate1();
    delegate2 = new ItemDelegate2();

    treeView->setItemDelegate(delegate2);
    treeView_2->setItemDelegate(delegate2);
    treeView_3->setItemDelegate(delegate);

    //--------Hide Vertical Scroll Bar---------------------------//
    treeView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    treeView_2->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    //--------Hide Horizontal Scroll Bar-------------------------//
    treeView->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    treeView_2->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    treeView_3->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);

    //-----------------------Mini Splitter-----------------------//
    splitter1 = new Manhattan::MiniSplitter();

    //----------------Setup display list signal------------//
    add->setText("Addend");
    add->setToolTip("Add signals at end of list signal in waveform");
    insert->setText("Insert");
    insert->setToolTip("Insert signals at below selected signal in waveform");
    remove->setText("Remove");
    remove->setToolTip("Remove selected signals from waveform");
    labelfilter->setText("Filter:");
    layoutbutton->insertWidget(0,add);
    layoutbutton->insertWidget(1,insert);
    layoutbutton->insertWidget(2,remove);
    layoutfilter->insertWidget(0,labelfilter);
    layoutdisplay->addWidget(listsignal);
    layoutdisplay->addLayout(layoutbutton);
    displaysignal = new QGroupBox;
    displaysignal->setTitle("Signals");
    displaysignal->setLayout(layoutdisplay);

    splitter1->insertWidget(0,displaysignal);
    splitter1->insertWidget(1,treeView);
    splitter1->insertWidget(2,treeView_2);
    splitter1->insertWidget(3,treeView_3);

    QList<int> heights;
    heights.push_back(300);
    heights.push_back(150);
    heights.push_back(150);
    heights.push_back(600);

    splitter1->setSizes(heights);
    splitter1->setStretchFactor(0,1);
    splitter1->setStretchFactor(1,0);
    splitter1->setStretchFactor(2,3);
    splitter1->setStretchFactor(3,1);
    splitter1->setFrameShape(QFrame::Box);
    splitter1->setLineWidth(2);
    splitter1->setStyleSheet("QSplitter  { color:rgb(191, 187, 187);}");

    //-----------Edit Splitter & Tree View Style Sheet --------------//
    treeView->setStyleSheet("QTreeView  { border-style: none;}");
    treeView_2->setStyleSheet("QTreeView  { border-style: none;}");
    treeView_3->setStyleSheet("QTreeView  { border-style: none;}");
    treeView_3->setAcceptDrops(true);

}

//Cai dat chuc nang cho cac menu item
void Dialog::createActions()
{
    //--------------------File Actions-----------------------------//
    openAct = new QAction(QIcon(":/images/open.png"), tr("&Open..."), this);
    openAct->setShortcuts(QKeySequence::Open);
    openAct->setStatusTip(tr("Open an existing file"));
    mainwindow->connect(openAct, SIGNAL(triggered()), this, SLOT(open()));

    //-------------------- Edit Actions-----------------------------------//
    zoomInAct = new QAction(QIcon(":/images/Gnome-Zoom-In-48.png"),tr("&Zoom In"), this);
    zoomInAct->setShortcut(QKeySequence::ZoomIn);
    zoomInAct->setStatusTip(tr("Zoom In"));

    zoomOutAct = new QAction(QIcon(":/images/Gnome-Zoom-Out-48.png"),tr("&Zoom Out"), this);
    zoomOutAct->setShortcut(QKeySequence::ZoomOut);
    zoomOutAct->setStatusTip(tr("Zoom Out"));

    zoomFitAct = new QAction(QIcon(":/images/Gnome-Zoom-Fit-Best-48.png"), tr("&Zoom Fit"), this);
    zoomFitAct->setStatusTip(tr("Zoom Fit"));
}

//Them cac shortcut o ToolBar
void Dialog::createToolBars()
{
    timeFromLabel = new QLabel(tr("From:"));
    timeFrom = new QLineEdit();
    timeToLabel = new QLabel(tr(" To"));
    timeTo = new QLineEdit();
    timeScaleLabel = new QLabel();
    cursorLabel = new QLabel();

    resizeToolbar = mainwindow->addToolBar(tr("&Resize"));
    resizeToolbar->addAction(zoomInAct);
    resizeToolbar->addAction(zoomOutAct);
    resizeToolbar->addAction(zoomFitAct);

    editToolbar = mainwindow->addToolBar(tr("&File"));
    editToolbar->addAction(openAct);

    timeToolbar = mainwindow->addToolBar(tr("&Time"));
    timeToolbar->addWidget(timeFromLabel);
    timeToolbar->addWidget(timeFrom);
    timeToolbar->addWidget(timeToLabel);
    timeToolbar->addWidget(timeTo);
    timeToolbar->addSeparator();
    timeToolbar->addWidget(timeScaleLabel);
    timeFrom->setDisabled(true);
    timeTo->setDisabled(true);

    currentCursor = mainwindow->addToolBar(tr("&Cursor"));
    currentCursor->addWidget(cursorLabel);
}

void Dialog::updateCursor()
{
    QModelIndex index = treeView_3->currentIndex();

    int column = index.column() - 1;

    cursorLabel->setText(tr("Cursor: %0 %1").arg(column).arg(vcdfile->getTimeUnit()));
}

void Dialog::updateTreeView()
{
    QModelIndex index = treeView_3->currentIndex(); // Lay index

    int column = index.column();

    treeView_3->setDrawColumn(column);
    treeView_3->viewport()->update();

    //-------An tat ca tru bien column---------//
    for (int i = 0; i < column_g; ++i)
    {
        if (i!=column)
        {
            treeView_2->hideColumn(i);  // ẩn tất cả các cột khác i
        }
        else
        {
            treeView_2->showColumn(i);  // hiện cột i
        }
    }
}

void Dialog::zoomInTreeView()
{

    for (int i = 0; i < column_g; ++i)
    {
        treeView_3->setColumnWidth(i, treeView_3->columnWidth(i) * 2);
    }
}

void Dialog::zoomOutTreeView()
{
    for (int i = 0; i < column_g; ++i)
    {
        treeView_3->setColumnWidth(i, treeView_3->columnWidth(i) / 2);
    }
}

void Dialog::zoomFitTreeView()
{
    int tree_width = splitter1->sizes()[3];
    for (int i = 0; i < column_g; ++i)
    {
        treeView_3->setColumnWidth(i, tree_width/(column_g - 1));
    }
}

void Dialog::buttonAddAction()
{
    QStringList list;
    foreach(const QModelIndex &index, listsignal->selectionModel()->selectedIndexes())
    {
        QString str = index.data(Qt::DisplayRole ).toString();
        int pos = str.lastIndexOf(" [");
        if (pos != -1) str.truncate(pos);
        list.append(str);
    }
    int numberSignal = vcdfile->numberOfSignal();
    int totalTime = vcdfile->getCycle() + 1;
    if (numberSignalinWaveform == 0) model->takeRow(0);
    for(int i = 0; i < list.size(); i++)
    {
        for(int r = 0; r < numberSignal; r++)                                   // Tạo model theo hàng : tức là bus
        {
            if (vcdfile->getSignal(r)->getLabel() == list.at(i))
            {
                QList<QStandardItem *> bus_i;                                   // Bus
                int bitWidth = vcdfile->getSignal(r)->getBitWidth();
                //{--------------------------Name Bus---------------------------//
                QStandardItem *bus_name = new QStandardItem();                  // Ten Bus
                QString bus_name_s;
                if (bitWidth > 1) bus_name_s = vcdfile->getSignal(r)->getLabel() + tr("[%0:0]").arg(bitWidth - 1);
                bus_name->setIcon(QIcon(":/images/Im-Yahoo-48.png"));
                bus_name->setText(bus_name_s);
                bus_name->setTextAlignment(Qt::AlignRight);
                bus_name->setEditable(false);
                bus_name->setToolTip(bus_name_s);
                //} Name Bus

                // Tạo số bit tương ứng với bus
                if (bitWidth > 1)
                {
                    QString bit_name_s;
                    for(int b = 0; b < bitWidth; ++b)
                    {
                        QStandardItem *bit_name = new QStandardItem();   // Hang Bit;
                        bit_name_s = vcdfile->getSignal(r)->getLabel() + tr("[%0]").arg(bitWidth - 1 - b);
                        bit_name->setText(bit_name_s);
                        bit_name->setTextAlignment(Qt::AlignRight);
                        bit_name->setEditable(false);
                        bit_name->setToolTip(bit_name_s);
                        bus_name->appendRow(bit_name);
                    }
                    bus_i.append(bus_name);
                }
                else
                {
                    QStandardItem *bus_name = new QStandardItem();
                    bus_name->setIcon(QIcon(":/images/Im-Yahoo-48.png"));
                    bus_name->setText(vcdfile->getSignal(r)->getLabel());
                    bus_name->setTextAlignment(Qt::AlignRight);
                    bus_name->setEditable(false);
                    bus_name->setToolTip(bus_name_s);
                    bus_i.append(bus_name);
                }

                //---------------------------Value Bus----------------------------//
                for(int c = 1; c < totalTime; c++)
                {
                    QStandardItem *bus_value = new QStandardItem(); // Gia tri Bus
                    QString busValue = vcdfile->getSignal(r)->getValue(c-1);
                    if (busValue[0] == 'b') busValue.remove(0,1);
                    bus_value->setText(busValue);
                    bus_value->setTextAlignment(Qt::AlignRight);
                    bus_value->setEditable(false);
                    bus_value->setToolTip(busValue);
                    bus_i.append(bus_value);
                    // Tạo giá trị bit tương ứng với bus
                    //--------------------------Value Bit--------------------------//
                    for ( int b = 0; b < bitWidth; ++b )
                    {
                        QStandardItem *bit_value = new QStandardItem();   // Hang Bit;
                        QChar bitc;
                        bitc = busValue[b];
                        QString bit_value_s = QString(bitc);
                        bit_value->setText(bit_value_s);
                        bit_value->setTextAlignment(Qt::AlignRight);
                        bit_value->setEditable(false);
                        bit_value->setToolTip(bit_value_s);

                        bus_name->setChild(b,c,bit_value);
                    }
                }
                model->insertRow(numberSignalinWaveform,bus_i);      // Add Bus
                numberSignalinWaveform++;
            }
        }
    }
}

void Dialog::buttonInsertAction()
{
    int pos = treeView_2->currentIndex().row() + 1; //vi tri chon tin hieu can chen
    QStringList list;
    foreach(const QModelIndex &index, listsignal->selectionModel()->selectedIndexes())
    {
        QString str = index.data(Qt::DisplayRole ).toString();
        int pos = str.lastIndexOf(" [");
        if (pos != -1) str.truncate(pos);
        list.append(str);
    }
    int numberSignal = vcdfile->numberOfSignal();
    int totalTime = vcdfile->getCycle() + 1;
    if (numberSignalinWaveform == 0) model->takeRow(0);
    for(int i = 0; i < list.size(); i++)
    {
        for(int r = 0; r < numberSignal; r++)                                   // Tạo model theo hàng : tức là bus
        {
            if (vcdfile->getSignal(r)->getLabel() == list.at(i))
            {
                QList<QStandardItem *> bus_i;                                   // Bus
                int bitWidth = vcdfile->getSignal(r)->getBitWidth();
                //{--------------------------Name Bus---------------------------//
                QStandardItem *bus_name = new QStandardItem();                  // Ten Bus
                QString bus_name_s;
                if (bitWidth > 1) bus_name_s = vcdfile->getSignal(r)->getLabel() + tr("[%0:0]").arg(bitWidth - 1);
                bus_name->setIcon(QIcon(":/images/Im-Yahoo-48.png"));
                bus_name->setText(bus_name_s);
                bus_name->setTextAlignment(Qt::AlignRight);
                bus_name->setEditable(false);
                bus_name->setToolTip(bus_name_s);
                //} Name Bus

                // Tạo số bit tương ứng với bus
                if (bitWidth > 1)
                {
                    QString bit_name_s;
                    for(int b = 0; b < bitWidth; ++b)
                    {
                        QStandardItem *bit_name = new QStandardItem();   // Hang Bit;
                        bit_name_s = vcdfile->getSignal(r)->getLabel() + tr("[%0]").arg(bitWidth - 1 - b);
                        bit_name->setText(bit_name_s);
                        bit_name->setTextAlignment(Qt::AlignRight);
                        bit_name->setEditable(false);
                        bit_name->setToolTip(bit_name_s);
                        bus_name->appendRow(bit_name);
                    }
                    bus_i.append(bus_name);
                }
                else
                {
                    QStandardItem *bus_name = new QStandardItem();
                    bus_name->setIcon(QIcon(":/images/Im-Yahoo-48.png"));
                    bus_name->setText(vcdfile->getSignal(r)->getLabel());
                    bus_name->setTextAlignment(Qt::AlignRight);
                    bus_name->setEditable(false);
                    bus_name->setToolTip(bus_name_s);
                    bus_i.append(bus_name);
                }

                //---------------------------Value Bus----------------------------//
                for(int c = 1; c < totalTime; c++)
                {
                    QStandardItem *bus_value = new QStandardItem(); // Gia tri Bus
                    QString busValue = vcdfile->getSignal(r)->getValue(c-1);
                    if (busValue[0] == 'b') busValue.remove(0,1);
                    bus_value->setText(busValue);
                    bus_value->setTextAlignment(Qt::AlignRight);
                    bus_value->setEditable(false);
                    bus_value->setToolTip(busValue);
                    bus_i.append(bus_value);
                    // Tạo giá trị bit tương ứng với bus
                    //--------------------------Value Bit--------------------------//
                    for ( int b = 0; b < bitWidth; ++b )
                    {
                        QStandardItem *bit_value = new QStandardItem();   // Hang Bit;
                        QChar bitc;
                        bitc = busValue[b];
                        QString bit_value_s = QString(bitc);
                        bit_value->setText(bit_value_s);
                        bit_value->setTextAlignment(Qt::AlignRight);
                        bit_value->setEditable(false);
                        bit_value->setToolTip(bit_value_s);

                        bus_name->setChild(b,c,bit_value);
                    }
                }
                model->insertRow(pos,bus_i);      // Add Bus
                pos++;
                numberSignalinWaveform++;
            }
        }
    }
}

void Dialog::buttonRemoveAction()
{
    int pos;    //vi tri chon tin hieu can xoa
    int totalTime = vcdfile->getCycle() + 1;
    for (int i = 0; i < treeView_2->selectionModel()->selectedIndexes().size(); i++)
    {
        pos = treeView_2->selectionModel()->selectedIndexes().at(i).row();
        model->takeRow(pos);
        numberSignalinWaveform--;
    }
    if (numberSignalinWaveform == 0)
    {
        QList<QStandardItem *> bus_i;                                   // Bus
        int bitWidth = 1;
        //{--------------------------Name Bus---------------------------//
        QStandardItem *bus_name = new QStandardItem();                  // Ten Bus
        QString bus_name_s = "";
        bus_name->setIcon(QIcon(":/images/Im-Yahoo-48.png"));
        bus_name->setText(bus_name_s);
        bus_name->setTextAlignment(Qt::AlignRight);
        bus_name->setEditable(false);
        bus_name->setToolTip(bus_name_s);
        QString bit_name_s;
        QStandardItem *bit_name = new QStandardItem();   // Hang Bit;
        bit_name_s = "";
        bit_name->setText(bit_name_s);
        bit_name->setTextAlignment(Qt::AlignRight);
        bit_name->setEditable(false);
        bit_name->setToolTip(bit_name_s);
        bus_i.append(bit_name);

        for (int c = 1; c < totalTime; c++)
        {
            QStandardItem *bus_value = new QStandardItem();             // Gia tri Bus
            QString busValue = "";
            bus_value->setText(busValue);
            bus_value->setTextAlignment(Qt::AlignRight);
            bus_value->setEditable(false);
            bus_value->setToolTip(busValue);
            bus_i.append(bus_value);
            for (int b = 0; b < bitWidth; ++b)
            {
                QStandardItem *bit_value = new QStandardItem();         // Hang Bit;
                QChar bitc;
                bitc = busValue[b];
                QString bit_value_s = QString(bitc);
                bit_value->setText(bit_value_s);
                bit_value->setTextAlignment(Qt::AlignRight);
                bit_value->setEditable(false);
                bit_value->setToolTip(bit_value_s);
                bus_name->setChild(b,c,bit_value);
            }
        }
        model->insertRow(0,bus_i);      // Add Bus
    }
}

void Dialog::treeView_2_ContextMenu(const QPoint &pos)
{
    // for most widgets
    QPoint globalPos = treeView_2->mapToGlobal(pos);
    QMenu myMenu;
    myMenu.addAction("Binary");
    myMenu.addAction("Hexa");
    QAction* selectedItem = myMenu.exec(globalPos);
    if (selectedItem != NULL)
    {
        if (selectedItem->text() == "Binary")
        {
            //Hien thi so tren waveform dang nhi phan
            treeView_3->setItemDelegate(delegate);
        }
        else if (selectedItem->text() == "Hexa")
        {
             //Hien thi so tren waveform dang hex
            treeView_3->setItemDelegate(delegate1);
        }
    }
}

