#include "signal.h"

Signal::Signal()
{

}

Signal::Signal(unsigned int _bitWidth, QString _name, QString _label)
{
    name = _name;
    label = _label;
    bitWidth = _bitWidth;
    value[0].resize(_bitWidth);
    for (unsigned int i = 0; i< _bitWidth; i++) value[0].append(LOW);
}

Signal::~Signal()
{

}

void Signal::setBitWidth(unsigned int _bitwidth)
{
    bitWidth = _bitwidth;
}

unsigned int Signal::getBitWidth()
{
    return bitWidth;
}

void Signal::setName(QString _name)
{
    name = _name;
}

QString Signal::getName()
{
    return name;
}

void Signal::setLabel(QString _label)
{
    label = _label;
}

QString Signal::getLabel()
{
    return label;
}

//Gan gia tri signal = value tai thoi diem _time
void Signal::setValue(unsigned int _time, QString _value)
{
    //_value duoc truyen vao phai co dang chuan:
    //  Neu la 1 bit: chi co 1 ki tu la so, vd: 1 / 0 / x
    //  Neu la nhieu bit: co ki tu b va theo sau la chuoi ki tu, vd: b1001 / bxxx1
    //Gan gia tri cho signal tu MSB -> LSB
    int length = _value.length();
    if (_value[0]=='b')
    {
        for (unsigned int i = 0; i < bitWidth; i++)
        {
            if (_value[length-1-i]=='0') value[_time].insert(i,LOW);
            else if (_value[length-1-i]=='1') value[_time].insert(i,HIGH);
            else if (_value[length-1-i]=='x') value[_time].insert(i,DONTCARE);
        }
    }
    else
    {
        if (_value=="0") value[_time].insert(0,LOW);
        else if (_value=="1") value[_time].insert(0,HIGH);
        else if (_value=="x") value[_time].insert(0,DONTCARE);
    }

}

//Ham lay gia tri cua signal tai thoi diem _time, ham tra ve chuoi string mang gia tri cua signal
QString Signal::getValue(unsigned int _time)
{
    //QString tra ve co dang chuan:
    //  Neu la 1 bit: chi co 1 ki tu la so, vd: 1 / 0 / x
    //  Neu la nhieu bit: co ki tu b o truoc va theo sau la chuoi ki tu, vd: b1001 / bxxx1
    QString tempValue = "";
    if (bitWidth == 1)
    {
        if (value[_time].at(0) == LOW)  tempValue = "0";
        else if (value[_time].at(0) == HIGH) tempValue = "1";
        else if (value[_time].at(0) == DONTCARE) tempValue = "x";
    }
    else if (bitWidth > 1)
    {
        for (unsigned int i = 0; i < bitWidth; i++)
        {
            if (value[_time].at(i) == LOW)  tempValue.prepend("0");
            else if (value[_time].at(i) == HIGH) tempValue.prepend("1");
            else if (value[_time].at(i) == DONTCARE) tempValue.prepend("x");
        }
        tempValue.prepend("b");
    }
    return tempValue;
}

//Ham tra ve tong so chu ki mo phong
int Signal::getTotalTime()
{
    return value.size();
}
