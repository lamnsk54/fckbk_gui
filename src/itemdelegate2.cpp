#include "itemdelegate2.h"

ItemDelegate2::ItemDelegate2(QWidget *parent) :
    QItemDelegate(parent)
{
}

QSize ItemDelegate2::sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    return QSize(4,24);
}
