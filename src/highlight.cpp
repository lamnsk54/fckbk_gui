#include "highlight.h"

HighLight::HighLight(QTextDocument *parent)
    : QSyntaxHighlighter(parent)
{
    HighlightingRule rule;

    keywordFormat.setForeground(Qt::darkBlue);
    QStringList keywordPatterns;
    keywordPatterns << "\\bmodule\\b" << "\\balways\\b" << "\\bprocess\\b"
                    << "\\bassign\\b" << "\\bendfunction\\b"
                    << "\\bfunction\\b" << "\\bfor\\b" << "\\bif\\b"
                    << "\\bthen\\b" << "\\belse\\b" << "\\bbegin\\b"
                    << "\\bend\\b" << "\\bendmodule\\b" << "\\table\\b" << "\\bendtable\\b"
                    << "\\binitial\\b" << "\\bwhile\\b" << "\\bcase\\b"
                    << "\\bforever\\b" ;
    foreach (const QString &pattern, keywordPatterns) {
        rule.pattern = QRegExp(pattern);
        rule.format = keywordFormat;
        highlightingRules.append(rule);
    }
    typewordFormat.setForeground(Qt::blue);
    typewordFormat.setFontItalic(true);
    QStringList typewordPatterns;
    typewordPatterns << "\\binput\\b" << "\\boutput\\b" << "\\binout\\b"
                    << "\\bwire\\b" << "\\breg\\b" << "\\bposedge\\b"
                    << "\\bnegedge\\b";
    foreach (const QString &pattern, typewordPatterns) {
        rule.pattern = QRegExp(pattern);
        rule.format = typewordFormat;
        highlightingRules.append(rule);
    }

    singleLineCommentFormat.setForeground(Qt::darkGreen);
    rule.pattern = QRegExp("//[^\n]*");
    rule.format = singleLineCommentFormat;
    highlightingRules.append(rule);

    multiLineCommentFormat.setForeground(Qt::darkGreen);
    quotationFormat.setForeground(Qt::darkGreen);
    rule.pattern = QRegExp("\".*\"");
    rule.format = quotationFormat;
    highlightingRules.append(rule);

    commentStartExpression = QRegExp("/\\*");
    commentEndExpression = QRegExp("\\*/");
}
void HighLight::highlightBlock(const QString &text)
{
    foreach (const HighlightingRule &rule, highlightingRules) {
        QRegExp expression(rule.pattern);
        int index = expression.indexIn(text);
        while (index >= 0) {
            int length = expression.matchedLength();
            setFormat(index, length, rule.format);
            index = expression.indexIn(text, index + length);
        }
    }
    setCurrentBlockState(0);
    int startIndex = 0;
    if (previousBlockState() != 1)
        startIndex = commentStartExpression.indexIn(text);

    while (startIndex >= 0) {
        int endIndex = commentEndExpression.indexIn(text, startIndex);
        int commentLength;
        if (endIndex == -1) {
            setCurrentBlockState(1);
            commentLength = text.length() - startIndex;
        } else {
            commentLength = endIndex - startIndex
                            + commentEndExpression.matchedLength();
        }
        setFormat(startIndex, commentLength, multiLineCommentFormat);
        startIndex = commentStartExpression.indexIn(text, startIndex + commentLength);
    }
}
