#ifndef ITEMDELEGATE2_H
#define ITEMDELEGATE2_H

#include <QItemDelegate>
#include <QtCore>
#include <QtGui>

/* Class Thừa kế từ Delegate
 * để thay đổi cách hiển thị của các Item trong TreeView 1 và 2
 */

class ItemDelegate2 : public QItemDelegate
{
    Q_OBJECT
public:
    explicit ItemDelegate2(QWidget *parent = 0);

    QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const;

    
signals:
    
public slots:
    
};

#endif // ITEMDELEGATE2_H
