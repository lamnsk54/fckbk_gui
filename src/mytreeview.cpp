#include "mytreeview.h"

myTreeView::myTreeView()
{

}

myTreeView::~myTreeView()
{

}

void myTreeView::paintEvent(QPaintEvent *event)
{
    QTreeView::paintEvent(event);
    QPainter painter(viewport());
    painter.setPen(QPen(Qt::yellow,1));
    for (int i = 0; i < header()->count(); ++i)
    {
         //draw only visible sections starting from second column
        if(header()->isSectionHidden(i) || header()->visualIndex(i) <= 0)
            continue;

         //position mapped to viewport
        int pos = header()->sectionViewportPosition(column) - 1;
        if (pos > 0)
            painter.drawLine(QPoint(pos+2, 0), QPoint(pos+2, height()));
    }
}


void myTreeView::setDrawColumn(int _column)
{
    column = _column;
}

int myTreeView::getDrawColumn()
{
    return column;
}
