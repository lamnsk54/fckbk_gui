#include "fsmproxy.h"

FSMProxy::FSMProxy(QObject *parent) :
    QSortFilterProxyModel(parent)
{
}

bool FSMProxy::filterAcceptsColumn(int source_column, const QModelIndex &source_parent) const
{
    Q_UNUSED(source_parent)

    // adjust the columns you want to filter out here
    // false: ẩn cột
    // true: hiện cột

    if (source_column !=0)      // chỉ lọc cột 0
    {
        return false;
    }
    return true;
}
