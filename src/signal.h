#ifndef DATAVCD_H
#define DATAVCD_H
#include <QVector>
#include <QMap>
#include <QString>

/***********************************************************
 * Class dinh nghia kieu du lieu cho tin hieu trong file VCD
 ***********************************************************/

//Dinh nghia cac kieu bien moi de luu du lieu trong file VCD
// Kiểu giá trị mới để lưu giá trị biến
enum lValue {LOW, HIGH, DONTCARE};// kieu gia tri moi de luu gia tri bien
/*
// Các kiểu dữ liệu trong file vcd
enum typevar{integer,parameter,real,reg,supply0,supply1,tri,triand,trior,trireg,tri0,tri1,wand,wire,wor};// kieu du lieu trong file vcd
*/

class Signal
{
public:
    Signal();
    Signal(unsigned int _bitWidth, QString _name, QString _label);
    ~Signal();
    /*
    void setType(typevar _type);
    typevar getType();
    */

    void setBitWidth(unsigned int _bitwidth);
    unsigned int getBitWidth();

    void setName(QString _name);
    QString getName();

    void setLabel(QString _label);
    QString getLabel();

    void setValue(unsigned int _time,QString _value);   //Gan gia tri signal = value tai thoi diem _time
    QString getValue(unsigned int _time);               //Ham lay gia tri cua signal tai thoi diem _time,
                                                        //Ham tra ve chuoi vector mang gia tri cua signal
    int getTotalTime();                                 //Ham tra ve tong so chu ki mo phong


private:
    //typevar type;         // kieu signal
    unsigned int bitWidth;  // kich thuoc signal
    QString name;           // ten cua signal, dai dien bang 2 ki tu trong bang ma ascii
    QString label;          // nhan cua signal
    QMap<unsigned int, QVector<lValue> > value; // kieu QMap luu thoi diem va gia tri bien
};

#endif // DATAVCD_H
