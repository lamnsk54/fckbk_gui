#ifndef ITEMDELEGATE_H
#define ITEMDELEGATE_H

#include <QItemDelegate>
#include <QtCore>
#include <QtGui>
#include <stdio.h>
#include <cstring>


/* Class Thừa kế từ Delegate
 * để thay đổi cách hiển thị của các Item trong TreeView 3
 */

class ItemDelegate : public QItemDelegate
{
    Q_OBJECT
public:
    explicit ItemDelegate(QWidget *parent = 0);

    QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const;

    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;

signals:
    
public slots:
    
};

#endif // ITEMDELEGATE_H
