#include "vcd.h"

VCD::VCD()
{
}

VCD::VCD(QString filename)
{
    ReadVCD(filename);
}

VCD::~VCD()
{
    delete []arrayValue;
}

void VCD::setTimeScale(unsigned _timescale)
{
    timescale = _timescale;
}

unsigned VCD::getTimeScale()
{
    return timescale;
}

void VCD::setTimeUnit(QString _timeUnit)
{
    timeUnit = _timeUnit;
}

QString VCD::getTimeUnit()
{
    return timeUnit;
}

Signal* VCD::getSignal(QString _name)    //ham lay ra 1 signal co name = _name
{
    for (int i = 0; i < vcdSignals.size(); i++)
        if (vcdSignals[i].getName() == _name) return &vcdSignals[i];
    return NULL;
}

Signal* VCD::getSignal(int pos) //ham lay ra 1 signal co vi tri pos trong vector vcdSignals
{
    if(pos < vcdSignals.size()) return &vcdSignals[pos];
    else return NULL;
}

void VCD::createSignal(unsigned int _bitWidth,QString _name, QString _label)   //ham khoi tao 1 signal
{
    Signal newsignal(_bitWidth, _name, _label);
    vcdSignals.append(newsignal);
}

bool VCD::ReadVCD(QString filename)      // ham doc file vcd co ten filename
{
    QFile vcdfile(filename);
    if (!vcdfile.open(QIODevice::ReadOnly | QIODevice::Text)) return false;
    unsigned int _bitWidth; //Bien tam de luu gia tri bitWidth truoc khi truyen cho signal
    QString _name;          //Bien tam de luu gia tri name truoc khi truyen cho signal
    QString _label;         //Bien tam de luu gia tri label truoc khi truyen cho signal
    QString _value;         //Bien tam de luu gia tri value truoc khi truyen cho signal, value o dang chuan
    unsigned int _oldtime = 0;  //Bien luu thoi gian cu ma gia tri signal thay doi
    unsigned int _time;     //Bien luu thoi gian ma gia tri signal thay doi

    QVector<QString> definition;    //Vector luu cac word nhan duoc trong 1 dong, da loai bo dau cach
    QTextStream in(&vcdfile);
    while (!in.atEnd())
    {
        QString str="";
        in >> str;
        if (str != "")
        {
            if (str == "$end")
            {
                if (definition[0] == "$var")
                {
                    bool ok;    //Bien nay chi de chuyen kieu QString sang Int, ko co y nghia
                    //Gan cac khai bao o trong file vcd cho cac signal
                    _bitWidth = definition[2].toInt(&ok);
                    _name = definition[3];
                    _label = definition[4];
                    createSignal(_bitWidth,_name,_label);
                    //xoa cac word da luu vao vector definition
                    definition.resize(0);
                }
                else if (definition[0] == "$date" || definition[0] == "$version")
                {
                    //xoa cac word da luu vao vector definition
                    definition.resize(0);
                }
                else if (definition[0] == "$timescale")
                {
                    QString _timeUnit;
                    unsigned _timeScale;
                    int pos;
                    //Kiem tra don vi thoi gian trong xau nhan duoc
                    if ((pos=definition[1].indexOf("ms")) != -1) _timeUnit = "ms";
                    else if ((pos=definition[1].indexOf("us")) != -1) _timeUnit = "us";
                    else if ((pos=definition[1].indexOf("ns")) != -1) _timeUnit = "ns";
                    else if ((pos=definition[1].indexOf("ps")) != -1) _timeUnit = "ps";
                    else if ((pos=definition[1].indexOf("fs")) != -1) _timeUnit = "fs";
                    else if ((pos=definition[1].indexOf("as")) != -1) _timeUnit = "as";
                    else if ((pos=definition[1].indexOf("zs")) != -1) _timeUnit = "zs";
                    else if ((pos=definition[1].indexOf("ys")) != -1) _timeUnit = "ys";
                    else
                    {
                        _timeUnit = "s";
                        pos = definition[1].length()-2;
                    }

                    definition[1].truncate(pos);
                    bool ok;    //Bien nay chi de chuyen kieu QString sang Int, ko co y nghia
                    _timeScale = definition[1].toInt(&ok);
                    setTimeUnit(_timeUnit);
                    setTimeScale(_timeScale);

                    //xoa cac word da luu vao vector definition
                    definition.resize(0);
                }
                else if (definition[0] == "$enddefinitions")
                {
                    //xoa cac word da luu vao vector definition
                    definition.resize(0);
                    break;
                }
            }
            else definition.append(str);
        }
    }

    //Truyen cac gia tri tai cac chu ki cho tin hieu
    while (!in.atEnd())
    {
        QString str="";
        str = in.readLine();
        if (str != "")
        {
            while (str[0] == ' ') str.remove(0,1);  //Xoa dau cach o dau str
            while (str[str.length()-1] == ' ') str.remove(str.length()-1,1);    //Xoa dau cach o cuoi str
            if (str[0] == '#')
            {
                //Nhan dien xem la chu ki thu may

                str.remove(0,1);
                bool ok;
                _time = str.toInt(&ok);
                //Vong lap gan tat ca cac gia tri cua tin hieu ko duoc gan trong file vcd
                //la mac dinh theo chu ki truoc do
                for (unsigned int i = _oldtime + 1; i <= _time; i++)
                {
                    //Duyet tat ca cac signal co trong file vcd
                    for (int j = 0; j < vcdSignals.size(); j++)
                    {
                        vcdSignals[j].setValue(i,vcdSignals[j].getValue(i-1));
                    }
                }
                _oldtime = _time;
            }
            //Gan cac gia tri tai thoi diem _time cho cac signal
            else if (str[0] == '0' || str[0] == '1' || str[0] == 'x')
            {
                _value = str[0];    //_value chua gia tri cua tin hieu
                str.remove(0,1);    //str se chua ten cua signal
                //Tim signal trong file vcd co name = str
                for (int j = 0; j < vcdSignals.size(); j++)
                {
                    if (vcdSignals[j].getName() == str) vcdSignals[j].setValue(_time,_value);
                }
            }
            else if (str[0] == 'b')
            {
                _value = "";
                int posOfSpace = str.indexOf(' ');
                QString _tempName = str.right(str.length() - posOfSpace - 1);    //_tempName se chua ten cua signal
                str.truncate(posOfSpace);   //str chua gia tri cua tin hieu
                //Tim signal trong file vcd co name = str
                for (int j = 0; j < vcdSignals.size(); j++)
                {
                    if (vcdSignals[j].getName() == _tempName)
                    {
                        //Duyet gia tri de gan cho value khi gia tri ngan hon bitWidth
                        for (unsigned int k = 0; k < vcdSignals[j].getBitWidth(); k++)
                        {
                            if (str != "b")
                            {
                                _value.prepend(str.right(1));
                                str = str.remove(str.length()-1,1);
                            }
                            else _value.prepend("0");
                        }
                        _value.prepend("b");
                        vcdSignals[j].setValue(_time,_value);
                    }
                }
            }
        }
    }
    vcdfile.close();

    //Gan cac gia tri cho mang arrayValue
    int capacitor = getCycle() * numberOfSignal();
    arrayValue = new int[capacitor];
    for (int i = 0; i < numberOfSignal(); i++)
    {
        //Gan gia tri cua signal cho mang nhu sau:
        //  arrayValue[] = {signal1_at_time0,signal1_at_time1,...,signal1_at_timeN,
        //                  signal2_at_time0,signal2_at_time1,...,signal2_at_timeN,
        //                  signalM_at_time0,signalm_at_time1,...,signalm_at_timeN}
        for (int j = 0; j < getCycle(); j++)
        {

        }
    }
    return true;
}

int VCD::numberOfSignal()           //Tra ve so tin hieu co trong file VCD
{
    return vcdSignals.size();
}

//Ham tra ve tong so chu ki mo phong trong file VCD
int VCD::getCycle()
{
    return vcdSignals[0].getTotalTime();
}

int* VCD::getArrayValue()
{
    return arrayValue;
}
