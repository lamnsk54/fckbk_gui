#ifndef VCD_H
#define VCD_H
#include <QString>
#include <QVector>
#include <QTextStream>
#include <QFile>

#include "signal.h"
/*************************************************************
 * Class chua toan bo cac tin hieu trong file VCD, bao gom:
 *  - Timescale/unit time
 *  - Cac tin hieu trong file VCD
 *  - Cac gia tri cua tin hieu tai cac chu ki
 *************************************************************/

class VCD
{
public:
    VCD();
    VCD(QString filename);
    ~VCD();
    void setTimeScale(unsigned _timescale);
    unsigned getTimeScale();

    void setTimeUnit(QString _timeUnit);
    QString getTimeUnit();

    Signal* getSignal(QString _name);    //ham lay ra 1 signal co name = _name
    Signal* getSignal(int pos);    //ham lay ra 1 signal co vi tri pos trong vector vcdSignals
    void createSignal(unsigned int _bitWidth,QString _name, QString _label);   //ham khoi tao 1 signal

    bool ReadVCD(QString filename);      // ham doc file vcd co ten filename

    int numberOfSignal();           //Tra ve so tin hieu co trong file VCD

    int getCycle();                                 //Ham tra ve tong so chu ki mo phong

    int* getArrayValue();      //Ham tra ve mang 2 chieu chua cac gia tri

private:
    unsigned timescale;     //Do chia nho nhat cua thoi gian
    QString timeUnit;               //Don vi thoi gian
    QVector<Signal> vcdSignals;   // vector vcdSignals chua toan bo cac signal trong file vcd
    int* arrayValue;

};

#endif // VCD_H
