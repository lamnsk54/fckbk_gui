/*
 * Class co form, co chuc nang tao mot project FCKBK moi, form co chuc nang
 *  nhap ten va duong dan cho project
 */
#include "createproject.h"
#include "ui_createproject.h"

CreateProject::CreateProject(QWidget *parent) :
    QDialog(parent),
    newpro(new Ui::CreateProject)
{
    newpro->setupUi(this);
}

CreateProject::~CreateProject()
{
    delete newpro;
}
//Browse den vi tri muon tao project
void CreateProject::on_Browse_clicked()
{
    QString path = QFileDialog::getExistingDirectory(this);
    newpro->lineEdit_Path->setText(path);
}
//Huy khong tao project
void CreateProject::on_Cancel_clicked()
{
    this->close();
}
//Hoan thanh viec tao project
void CreateProject::on_Finish_clicked()
{
    QDir projectdir(newpro->lineEdit_Path->text() + "/" + newpro->lineEdit_Name->text());
    //begin: thong bao neu field name bi bo trong
    if(newpro->lineEdit_Name->text() == "")
        QMessageBox::warning(this,"Warning","Name is empty");
    //end: thong bao neu field name bi bo trong

    //begin: thong bao neu field path bi bo trong
    else if(newpro->lineEdit_Path->text() == "")
        QMessageBox::warning(this,"Warning","Path is empty");
    //end: thong bao neu field path bi bo trong

    //begin: thong bao neu project da ton tai
    else if(projectdir.exists())
        QMessageBox::warning(this,"Warning","Project is existing");
    //end: thong bao neu project da ton tai

    //begin: Tao project va cac subfolder
    else
    {
        projectpath = newpro->lineEdit_Path->text() + "/" + newpro->lineEdit_Name->text();
        projectdir.mkpath(projectpath);     //Tao thu muc co ten la ten project
        projectdir.mkpath(projectpath+"/Design");   //Tao thu muc Design trong project
        projectdir.mkpath(projectpath+"/Run");      //Tao thu muc Run trong project
        projectdir.mkpath(projectpath+"/Property"); //Tao thu muc Property trong project
        projectdir.mkpath(projectpath+"/Results");  //Tao thu muc Results trong project
        projectdir.mkpath(projectpath+"/Results/Circuit");  //Tao thu muc /Results/Circuit trong project
        projectdir.mkpath(projectpath+"/Results/LOG");      //Tao thu muc /Results/LOG trong project
        projectdir.mkpath(projectpath+"/Results/VCD");      //Tao thu muc /Results/VCD trong project
        this->close();
    }
    //end: Tao project va cac subfolder
}
