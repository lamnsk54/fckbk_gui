#include "myqtextedit.h"

myQTextEdit::myQTextEdit()
{
    QTextEdit();
    filepath = "";
}

myQTextEdit::~myQTextEdit()
{

}

void myQTextEdit::Setfilepath(const QString &path)
{
    filepath = path;
}

QString myQTextEdit::Getfilepath()
{
    return filepath;
}
void myQTextEdit::setupEditor()
{
    QFont font;
    font.setFamily("Courier");
    font.setFixedPitch(true);
    font.setPointSize(13);
    this->setFont(font);
    highlighter = new HighLight(this->document());
}
