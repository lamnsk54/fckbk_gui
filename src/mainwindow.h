#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtCore>
#include <QtGui>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QDataWidgetMapper>
#include <QTabWidget>
#include <QFileDialog>
#include <QLabel>
#include <QGroupBox>
#include <QTreeView>
#include <QListView>
#include <QDirModel>
#include <QTableView>
#include <QSpinBox>
#include <QtWidgets>
#include <QTextEdit>
#include <QPushButton>

#include <itemdelegate.h>
#include <itemdelegate2.h>
#include <fsmproxy.h>

//-----------STL---------------//

#include <stdio.h>
#include <stdlib.h>
#include <cstdlib>
#include <bitset>

//-----------Plugins-----------//
#include "minisplitter.h"
#include "manhattanstyle.h"
#include "styledbar.h"
#include "fancytabwidget.h"
#include "vcd.h"

enum {NumButtons = 5};

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    void createActions();
    void createMenus();
    void createToolBars();
    //void createFanxyTabBar();

    VCD *vcdfile;
    int numberSignalinWaveform;     //So cac tin hieu co trong waveform

    ///0----------------Menu ----------------//
    QMenu *fileMenu;
    QMenu *editMenu;
    //QMenu *signalsMenu;
    //QMenu *viewMenu;
    //QMenuBar *menuBar;
    ///0----------------Menu ----------------//

    ///1---------------Toolbar---------------//
    //QToolBar *pluginToolbar;
    QToolBar *resizeToolbar;
    QToolBar *editToolbar;
    QToolBar *timeToolbar;
    QToolBar *currentCursor;
    QLabel *timeFromLabel;
    QLabel *timeToLabel;
    QLabel *signalsLabel;
    QLabel *timeScaleLabel;
    QLabel *cursorLabel;
    QLineEdit *timeFrom;
    QLineEdit *timeTo;
    ///1---------------Toolbar---------------//

    ///2---------------Actions---------------//
    //QAction *newAct;
    //QAction *saveAct;
    //QAction *saveAsAct;
    //QAction *pluginAct;
    QAction *openAct;
    QAction *exitAct;
    QAction *zoomInAct;
    QAction *zoomOutAct;
    QAction *zoomFitAct;
    ///2---------------Actions---------------//

    ///3-------------WaveForm----------------//
    //int row_g;
    int column_g;
    //int item_g;
    ///3-------------WaveForm----------------//


    ///------------Functions--------------//

    //Hàm khởi tạo Model
    void setupModel(QStandardItemModel *model, QString pathVcdFile);

    // Hàm tạo Nhóm Tree View
    void createTreeView();

    ///4------------Nhóm Waveform Viewer----------//
    QStandardItemModel *model;
    QGroupBox *gridGroupBox;
    QHBoxLayout *hbox;

    QPalette *palette;
    QTreeView *treeView;
    QTreeView *treeView_2;
    QTreeView *treeView_3;
    ///4------------Nhóm Waveform Viewer----------//


    ///5----Nhom Display Signal Selection---------//
    QGroupBox *displaysignal;
    QVBoxLayout *layoutdisplay;
    QHBoxLayout *layoutbutton;
    QHBoxLayout *layoutfilter;
    QLabel *labelfilter;
    QLineEdit *textfilter;
    QListView *listsignal;
    QPushButton *add;
    QPushButton *insert;
    QStringListModel *QSLMSignal;
    QStringList listSignalDisplay;  //Bien luu tru label cac signal
    QAbstractItemModel *modelSignal;
    ///5----Nhom Display Signal Selection---------//


    FSMProxy *proxy;
    //QSplitter *splitter;
    //QSplitterHandle *handles;
    //QDirModel *dirmodel;
    ItemDelegate *delegate;
    ItemDelegate2 *delegate2;


    ///6--------------Menu Bar--------------------//
    QMenuBar *menuBar;
//    QMenu *fileMenu;
//    QAction *exitAction;
    ///6--------------Menu Bar--------------------//

    ///7--------------Nhóm nút bấm----------------//

    ///7--------------Nhóm nút bấm----------------//

    //QGroupBox *horizontalGroupBox;
    //QPushButton *buttons[NumButtons];

    //--------- Nhóm Plugins------------//

    Manhattan::MiniSplitter *splitter1;
    Manhattan::FancyTabBar *fancytab1;
    //Manhattan::FancyTab *newFancy;
    //Manhattan::FancyTab *openFancy;

private slots:
    void updateTreeView();      // update Tree View 2
    void zoomInTreeView();
    void zoomOutTreeView();
    void zoomFitTreeView();
    void updateCursor();        //update vi tri thanh gat
    void buttonAddAction();
    void buttonInsertAction();

    //-----------Open File--------------//

    void open();
};

#endif // MAINWINDOW_H
