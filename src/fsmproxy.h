#ifndef FSMPROXY_H
#define FSMPROXY_H

#include <QSortFilterProxyModel>

/* Class Thừa kế từ QSortFilterProxyModel
 * để thực hiện 2 chức năng sắp xếp và lọc dữ liệu trong model
 */

class FSMProxy : public QSortFilterProxyModel
{
    Q_OBJECT
public:
    explicit FSMProxy(QObject *parent = 0);

protected:

    bool filterAcceptsColumn(int source_column, const QModelIndex &source_parent) const;
    
signals:
    
public slots:
    
};

#endif // FSMPROXY_H
