/*Class co form, co nhiem vu hien thi hop thoai de nhap ten
 * file moi de add vao thu muc trong project cua FCKBK
 */
#include "addnewfile.h"
#include "ui_addnewfile.h"

addnewfile::addnewfile(QWidget *parent) :
    QDialog(parent),
    addnew(new Ui::addnewfile)
{
    addnew->setupUi(this);
}

addnewfile::~addnewfile()
{
    delete addnew;
}

//begin: phuong thuc huy khong tao file moi
void addnewfile::on_Cancel_clicked()
{
    this->close();
}
//end: phuong thuc huy khong tao file moi

//begin: phuong thuc tao file moi
void addnewfile::on_Create_clicked()
{
    bool flagtype = false;
    //begin: Thong bao neu truong namefile bi bo trong
    if(addnew->textline_namefile->text() == "") QMessageBox::warning(this,"Warning","File name is empty");
    //end: Thong bao neu truong namefile bi bo trong

    //begin: Kiem tra xem ten file duoc tao co phan mo rong khong
    for (int i = 0; i < addnew->textline_namefile->text().size(); ++i)
    {
        if (addnew->textline_namefile->text()[i] == '.')
        {
            flagtype = true;
        }
    }
    //Ten file duoc tao bat buoc phai co phan mo rong de hien thi o cua so treeview (cua so quan ly project)
    //end: Kiem tra xem ten file duoc tao co phan mo rong khong

    //begin: Kiem tra neu file khong co phan mo rong thi xuat hien hop thoai thong bao
    if (!flagtype) QMessageBox::warning(this,"Warning","The format of file is not available");
    else    //neu co phan mo rong thi tao file va dong hop thoai
    {
        setnamefile(addnew->textline_namefile->text());
        this->close();
    }
    //end: Kiem tra neu file khong co phan mo rong thi xuat hien hop thoai thong bao
}
//end: phuong thuc tao file moi

//Phuong thuc truy cap vao thuoc tinh namefile
void addnewfile::setnamefile(QString _namefile)
{
    namefile = _namefile;
}

//Phuong thuc truy cap vao thuoc tinh namefile
QString addnewfile::getnamefile()
{
    return namefile;
}
