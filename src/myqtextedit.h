#ifndef MYQTEXTEDIT_H
#define MYQTEXTEDIT_H
#include <QTextEdit>
#include <QString>
#include <QFont>

#include "highlight.h"

class myQTextEdit: public QTextEdit
{
public:
    myQTextEdit();
    ~myQTextEdit();
    void Setfilepath(const QString &path);
    QString Getfilepath();
    void setupEditor();
private:
    QString filepath;
    HighLight *highlighter;
};

#endif // MYQTEXTEDIT_H
