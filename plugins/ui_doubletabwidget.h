/********************************************************************************
** Form generated from reading UI file 'doubletabwidget.ui'
**
** Created by: Qt User Interface Compiler version 5.4.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DOUBLETABWIDGET_H
#define UI_DOUBLETABWIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QWidget>

namespace Manhattan {

class Ui_DoubleTabWidget
{
public:

    void setupUi(QWidget *Manhattan__DoubleTabWidget)
    {
        if (Manhattan__DoubleTabWidget->objectName().isEmpty())
            Manhattan__DoubleTabWidget->setObjectName(QStringLiteral("Manhattan__DoubleTabWidget"));
        Manhattan__DoubleTabWidget->resize(600, 400);
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(Manhattan__DoubleTabWidget->sizePolicy().hasHeightForWidth());
        Manhattan__DoubleTabWidget->setSizePolicy(sizePolicy);

        retranslateUi(Manhattan__DoubleTabWidget);

        QMetaObject::connectSlotsByName(Manhattan__DoubleTabWidget);
    } // setupUi

    void retranslateUi(QWidget *Manhattan__DoubleTabWidget)
    {
        Manhattan__DoubleTabWidget->setWindowTitle(QApplication::translate("Manhattan::DoubleTabWidget", "DoubleTabWidget", 0));
    } // retranslateUi

};

} // namespace Manhattan

namespace Manhattan {
namespace Ui {
    class DoubleTabWidget: public Ui_DoubleTabWidget {};
} // namespace Ui
} // namespace Manhattan

#endif // UI_DOUBLETABWIDGET_H
