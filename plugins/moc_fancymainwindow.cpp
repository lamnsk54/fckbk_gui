/****************************************************************************
** Meta object code from reading C++ file 'fancymainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.4.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "fancymainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'fancymainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.4.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_Manhattan__FancyMainWindow_t {
    QByteArrayData data[10];
    char stringdata[144];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Manhattan__FancyMainWindow_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Manhattan__FancyMainWindow_t qt_meta_stringdata_Manhattan__FancyMainWindow = {
    {
QT_MOC_LITERAL(0, 0, 26), // "Manhattan::FancyMainWindow"
QT_MOC_LITERAL(1, 27, 11), // "resetLayout"
QT_MOC_LITERAL(2, 39, 0), // ""
QT_MOC_LITERAL(3, 40, 9), // "setLocked"
QT_MOC_LITERAL(4, 50, 6), // "locked"
QT_MOC_LITERAL(5, 57, 21), // "setDockActionsVisible"
QT_MOC_LITERAL(6, 79, 1), // "v"
QT_MOC_LITERAL(7, 81, 21), // "onDockActionTriggered"
QT_MOC_LITERAL(8, 103, 22), // "onDockVisibilityChange"
QT_MOC_LITERAL(9, 126, 17) // "onTopLevelChanged"

    },
    "Manhattan::FancyMainWindow\0resetLayout\0"
    "\0setLocked\0locked\0setDockActionsVisible\0"
    "v\0onDockActionTriggered\0onDockVisibilityChange\0"
    "onTopLevelChanged"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Manhattan__FancyMainWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   44,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       3,    1,   45,    2, 0x0a /* Public */,
       5,    1,   48,    2, 0x0a /* Public */,
       7,    0,   51,    2, 0x08 /* Private */,
       8,    1,   52,    2, 0x08 /* Private */,
       9,    0,   55,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void, QMetaType::Bool,    4,
    QMetaType::Void, QMetaType::Bool,    6,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void,

       0        // eod
};

void Manhattan::FancyMainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        FancyMainWindow *_t = static_cast<FancyMainWindow *>(_o);
        switch (_id) {
        case 0: _t->resetLayout(); break;
        case 1: _t->setLocked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 2: _t->setDockActionsVisible((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 3: _t->onDockActionTriggered(); break;
        case 4: _t->onDockVisibilityChange((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 5: _t->onTopLevelChanged(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (FancyMainWindow::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&FancyMainWindow::resetLayout)) {
                *result = 0;
            }
        }
    }
}

const QMetaObject Manhattan::FancyMainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_Manhattan__FancyMainWindow.data,
      qt_meta_data_Manhattan__FancyMainWindow,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Manhattan::FancyMainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Manhattan::FancyMainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Manhattan__FancyMainWindow.stringdata))
        return static_cast<void*>(const_cast< FancyMainWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int Manhattan::FancyMainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 6)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 6;
    }
    return _id;
}

// SIGNAL 0
void Manhattan::FancyMainWindow::resetLayout()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}
QT_END_MOC_NAMESPACE
