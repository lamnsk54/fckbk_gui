/****************************************************************************
** Meta object code from reading C++ file 'fancylineedit.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.4.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "fancylineedit.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'fancylineedit.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.4.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_Manhattan__IconButton_t {
    QByteArrayData data[4];
    char stringdata[50];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Manhattan__IconButton_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Manhattan__IconButton_t qt_meta_stringdata_Manhattan__IconButton = {
    {
QT_MOC_LITERAL(0, 0, 21), // "Manhattan::IconButton"
QT_MOC_LITERAL(1, 22, 11), // "iconOpacity"
QT_MOC_LITERAL(2, 34, 8), // "autoHide"
QT_MOC_LITERAL(3, 43, 6) // "pixmap"

    },
    "Manhattan::IconButton\0iconOpacity\0"
    "autoHide\0pixmap"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Manhattan__IconButton[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       3,   14, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // properties: name, type, flags
       1, QMetaType::Float, 0x00095103,
       2, QMetaType::Bool, 0x00095103,
       3, QMetaType::QPixmap, 0x00095103,

       0        // eod
};

void Manhattan::IconButton::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObject Manhattan::IconButton::staticMetaObject = {
    { &QAbstractButton::staticMetaObject, qt_meta_stringdata_Manhattan__IconButton.data,
      qt_meta_data_Manhattan__IconButton,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Manhattan::IconButton::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Manhattan::IconButton::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Manhattan__IconButton.stringdata))
        return static_cast<void*>(const_cast< IconButton*>(this));
    return QAbstractButton::qt_metacast(_clname);
}

int Manhattan::IconButton::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QAbstractButton::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    
#ifndef QT_NO_PROPERTIES
     if (_c == QMetaObject::ReadProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< float*>(_v) = iconOpacity(); break;
        case 1: *reinterpret_cast< bool*>(_v) = hasAutoHide(); break;
        case 2: *reinterpret_cast< QPixmap*>(_v) = pixmap(); break;
        default: break;
        }
        _id -= 3;
    } else if (_c == QMetaObject::WriteProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: setIconOpacity(*reinterpret_cast< float*>(_v)); break;
        case 1: setAutoHide(*reinterpret_cast< bool*>(_v)); break;
        case 2: setPixmap(*reinterpret_cast< QPixmap*>(_v)); break;
        default: break;
        }
        _id -= 3;
    } else if (_c == QMetaObject::ResetProperty) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 3;
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        if (_id < 3)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 3;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}
struct qt_meta_stringdata_Manhattan__FancyLineEdit_t {
    QByteArrayData data[12];
    char stringdata[154];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Manhattan__FancyLineEdit_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Manhattan__FancyLineEdit_t qt_meta_stringdata_Manhattan__FancyLineEdit = {
    {
QT_MOC_LITERAL(0, 0, 24), // "Manhattan::FancyLineEdit"
QT_MOC_LITERAL(1, 25, 13), // "buttonClicked"
QT_MOC_LITERAL(2, 39, 0), // ""
QT_MOC_LITERAL(3, 40, 30), // "Manhattan::FancyLineEdit::Side"
QT_MOC_LITERAL(4, 71, 4), // "side"
QT_MOC_LITERAL(5, 76, 17), // "leftButtonClicked"
QT_MOC_LITERAL(6, 94, 18), // "rightButtonClicked"
QT_MOC_LITERAL(7, 113, 12), // "checkButtons"
QT_MOC_LITERAL(8, 126, 11), // "iconClicked"
QT_MOC_LITERAL(9, 138, 4), // "Side"
QT_MOC_LITERAL(10, 143, 4), // "Left"
QT_MOC_LITERAL(11, 148, 5) // "Right"

    },
    "Manhattan::FancyLineEdit\0buttonClicked\0"
    "\0Manhattan::FancyLineEdit::Side\0side\0"
    "leftButtonClicked\0rightButtonClicked\0"
    "checkButtons\0iconClicked\0Side\0Left\0"
    "Right"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Manhattan__FancyLineEdit[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       1,   48, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   39,    2, 0x06 /* Public */,
       5,    0,   42,    2, 0x06 /* Public */,
       6,    0,   43,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       7,    1,   44,    2, 0x08 /* Private */,
       8,    0,   47,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void,

 // enums: name, flags, count, data
       9, 0x0,    2,   52,

 // enum data: key, value
      10, uint(Manhattan::FancyLineEdit::Left),
      11, uint(Manhattan::FancyLineEdit::Right),

       0        // eod
};

void Manhattan::FancyLineEdit::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        FancyLineEdit *_t = static_cast<FancyLineEdit *>(_o);
        switch (_id) {
        case 0: _t->buttonClicked((*reinterpret_cast< Manhattan::FancyLineEdit::Side(*)>(_a[1]))); break;
        case 1: _t->leftButtonClicked(); break;
        case 2: _t->rightButtonClicked(); break;
        case 3: _t->checkButtons((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 4: _t->iconClicked(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (FancyLineEdit::*_t)(Manhattan::FancyLineEdit::Side );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&FancyLineEdit::buttonClicked)) {
                *result = 0;
            }
        }
        {
            typedef void (FancyLineEdit::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&FancyLineEdit::leftButtonClicked)) {
                *result = 1;
            }
        }
        {
            typedef void (FancyLineEdit::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&FancyLineEdit::rightButtonClicked)) {
                *result = 2;
            }
        }
    }
}

const QMetaObject Manhattan::FancyLineEdit::staticMetaObject = {
    { &QLineEdit::staticMetaObject, qt_meta_stringdata_Manhattan__FancyLineEdit.data,
      qt_meta_data_Manhattan__FancyLineEdit,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Manhattan::FancyLineEdit::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Manhattan::FancyLineEdit::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Manhattan__FancyLineEdit.stringdata))
        return static_cast<void*>(const_cast< FancyLineEdit*>(this));
    return QLineEdit::qt_metacast(_clname);
}

int Manhattan::FancyLineEdit::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QLineEdit::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 5)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 5;
    }
    return _id;
}

// SIGNAL 0
void Manhattan::FancyLineEdit::buttonClicked(Manhattan::FancyLineEdit::Side _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Manhattan::FancyLineEdit::leftButtonClicked()
{
    QMetaObject::activate(this, &staticMetaObject, 1, Q_NULLPTR);
}

// SIGNAL 2
void Manhattan::FancyLineEdit::rightButtonClicked()
{
    QMetaObject::activate(this, &staticMetaObject, 2, Q_NULLPTR);
}
QT_END_MOC_NAMESPACE
