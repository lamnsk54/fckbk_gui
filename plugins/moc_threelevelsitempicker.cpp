/****************************************************************************
** Meta object code from reading C++ file 'threelevelsitempicker.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.4.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "extensions/threelevelsitempicker.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'threelevelsitempicker.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.4.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_Manhattan__ListWidget_t {
    QByteArrayData data[1];
    char stringdata[22];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Manhattan__ListWidget_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Manhattan__ListWidget_t qt_meta_stringdata_Manhattan__ListWidget = {
    {
QT_MOC_LITERAL(0, 0, 21) // "Manhattan::ListWidget"

    },
    "Manhattan::ListWidget"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Manhattan__ListWidget[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void Manhattan::ListWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObject Manhattan::ListWidget::staticMetaObject = {
    { &QListWidget::staticMetaObject, qt_meta_stringdata_Manhattan__ListWidget.data,
      qt_meta_data_Manhattan__ListWidget,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Manhattan::ListWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Manhattan::ListWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Manhattan__ListWidget.stringdata))
        return static_cast<void*>(const_cast< ListWidget*>(this));
    return QListWidget::qt_metacast(_clname);
}

int Manhattan::ListWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QListWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
struct qt_meta_stringdata_Manhattan__ThreeLevelsItemPicker_t {
    QByteArrayData data[7];
    char stringdata[81];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Manhattan__ThreeLevelsItemPicker_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Manhattan__ThreeLevelsItemPicker_t qt_meta_stringdata_Manhattan__ThreeLevelsItemPicker = {
    {
QT_MOC_LITERAL(0, 0, 32), // "Manhattan::ThreeLevelsItemPicker"
QT_MOC_LITERAL(1, 33, 11), // "itemChanged"
QT_MOC_LITERAL(2, 45, 0), // ""
QT_MOC_LITERAL(3, 46, 9), // "setLevel1"
QT_MOC_LITERAL(4, 56, 4), // "name"
QT_MOC_LITERAL(5, 61, 9), // "setLevel2"
QT_MOC_LITERAL(6, 71, 9) // "setLevel3"

    },
    "Manhattan::ThreeLevelsItemPicker\0"
    "itemChanged\0\0setLevel1\0name\0setLevel2\0"
    "setLevel3"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Manhattan__ThreeLevelsItemPicker[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   34,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       3,    1,   35,    2, 0x08 /* Private */,
       5,    1,   38,    2, 0x08 /* Private */,
       6,    1,   41,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void, QMetaType::QString,    4,
    QMetaType::Void, QMetaType::QString,    4,
    QMetaType::Void, QMetaType::QString,    4,

       0        // eod
};

void Manhattan::ThreeLevelsItemPicker::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        ThreeLevelsItemPicker *_t = static_cast<ThreeLevelsItemPicker *>(_o);
        switch (_id) {
        case 0: _t->itemChanged(); break;
        case 1: _t->setLevel1((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 2: _t->setLevel2((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 3: _t->setLevel3((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (ThreeLevelsItemPicker::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&ThreeLevelsItemPicker::itemChanged)) {
                *result = 0;
            }
        }
    }
}

const QMetaObject Manhattan::ThreeLevelsItemPicker::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_Manhattan__ThreeLevelsItemPicker.data,
      qt_meta_data_Manhattan__ThreeLevelsItemPicker,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Manhattan::ThreeLevelsItemPicker::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Manhattan::ThreeLevelsItemPicker::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Manhattan__ThreeLevelsItemPicker.stringdata))
        return static_cast<void*>(const_cast< ThreeLevelsItemPicker*>(this));
    return QWidget::qt_metacast(_clname);
}

int Manhattan::ThreeLevelsItemPicker::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 4)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 4;
    }
    return _id;
}

// SIGNAL 0
void Manhattan::ThreeLevelsItemPicker::itemChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}
QT_END_MOC_NAMESPACE
